<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Historial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('idReserva');
          $table->integer('idHora');
          $table->integer('idDia');
          $table->integer('idCancha');
          $table->string('nombre');
          $table->string('telefono');
          $table->string('comentario')->nullable();
          $table->date('fecha');
          $table->boolean('jugado');
          $table->boolean('soloHoy');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial');
    }
}
