<?php

namespace App\Http\Controllers;

use App\TipoServicio;
use App\Servicio;
use Illuminate\Http\Request;
use DB;
use Session;

class ServicioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function cargarVistaAgregarServicio()
    {
        /*$art = DB::table('articulos')
        ->join('categorias', 'categorias.id', '=', 'articulos.idCategoria')
        ->select('articulos.*', 'categorias.titulo as tituloC')
        ->where('articulos.estado', 1)
        ->orderby('articulos.updated_at', 'desc')
        ->get();

        $artBorradorr = DB::table('articulos')
        ->where('articulos.estado', 0)->count();
        $conf = DB::table('configuracion')->get();*/
        $tipoS = DB::table('tipo_servicios')->get();
        return view('agregar_servicio', ['seccion' => 'agregarServicio', "tituloSección" => 'Agregar servicio', 'tipoS' =>$tipoS]);
    }

    public function cargarVistaListarServicio()
    {
        /*$art = DB::table('articulos')
        ->join('categorias', 'categorias.id', '=', 'articulos.idCategoria')
        ->select('articulos.*', 'categorias.titulo as tituloC')
        ->where('articulos.estado', 1)
        ->orderby('articulos.updated_at', 'desc')
        ->get();

        $artBorradorr = DB::table('articulos')
        ->where('articulos.estado', 0)->count();
        $conf = DB::table('configuracion')->get();*/
        $tipoS = DB::table('tipo_servicios')->get();
        $servicio = DB::table('servicios')->get();
        return view('listar_servicio', ['seccion' => 'listarServicio', "tituloSección" => 'Listar servicios', 'servicios' =>$servicio, 'tipoS' =>$tipoS]);
    }

    
    
    public function modificarServicio(Request $request)
    {
        $this->validate($request, ["nombre" => 'required|min:1',
        "precio" => 'required|min:1',
        "tipoS" => 'required|min:1'
        ]);

        DB::table('servicios')
            ->where('id', $request->id)
            ->update([
                'nombre' => $request->nombre,
                'precio' => $request->precio,
                'tipoServicio' => $request->tipoS
                ]);
        return response('Servicio modificado correctamente.', 200)
            ->header('Content-Type', 'text/plain');
    }
    public function eliminarServicio(Request $request)
    {
        DB::table('servicios')
            ->where('id', $request->id)
            ->delete();
        return response('Servicio eliminado correctamente.', 200)
            ->header('Content-Type', 'text/plain');
    }

    public function ingresarServicio(Request $request)
    {
        $this->validate($request, ["nombre" => 'required|min:1',
        "precio" => 'required|min:1',
        "selectTipoS" => 'required|min:1'
        ]);

        $servicio = new Servicio();
        $servicio->nombre = $request->nombre;
        $servicio->tipoServicio = $request->selectTipoS;
        $servicio->precio = $request->precio;
        

        if (!$servicio->save()) {
            return response('Error al ingresar el registro, intentelo nuevamente', 500)
                ->header('Content-Type', 'text/plain');
        } else {
            return response('Servicio ingresado correctamente.', 200)
                ->header('Content-Type', 'text/plain');
        }

    }    

    public function resizeImagePost($image)
  {

    //para llamar a esta funcion
    /*if($request->file('fileImagen') != null){
            $this->resizeImagePost($request->file('fileImagen'));
        }*/
   
    //$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
    $input['imagename'] = $image->getClientOriginalName();


    $destinationPath = public_path('static/thumbnail');
    $img = Image::make($image->getRealPath());
    $ancho = (35000/$img->width());
    $img->resize(350, (($img->height()*$ancho)/100), function ($constraint) {
      $constraint->aspectRatio();
    })->save($destinationPath.'/'.$input['imagename']);

    $destinationPath = public_path('static/images-panel');
    $image->move($destinationPath, $input['imagename']);
    /*Copia la imagen a original*/
    $destinationPathOrigin = public_path('static/images-original');
    \File::copy($destinationPath.'/'.$input['imagename'] , $destinationPathOrigin.'/'.$input['imagename'] );
    /*---------------*/
    return $this->postImage($image->getClientOriginalName());

  }

}
