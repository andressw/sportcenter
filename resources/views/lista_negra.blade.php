 @include('layouts.head_panel_resp_table')
<div class="contentCenter columns large-10 large-offset-2 medium-12 small-12">
    <div class="contentFormulario columns large-12 medium-12 small-12">
        <div class="columns large-12 medium-12 small-12 no-padding buttonCustomListaN">
            <button class="button buttonCustomListaN" type="button" onclick="desplegarAgregarListaNegra();">
                <i class="fa fa-plus" aria-hidden="true"></i> Agregar nuevo registro</button>
        </div>
        <!--<div class="row contenidoAgregarListaNegra rowModificarEliminar">

            <div class="columns large-4 medium-5 small-12">
                <div class="columns row">
                    <label class="labelNameInput">Nombre
                        <input id="nombre" type="text" placeholder="Nombre">
                    </label>
                </div>

                <div class="columns row">
                    <label class="labelNameInput">Teléfono
                        <input id="tel" type="text" placeholder="099999888">
                    </label>
                </div>
            </div>
            <div class="columns large-8 medium-7 small-12">
                <div class="columns row">
                    <label class="labelNameInput">Comentario
                        <textarea name="comentario" id="comentario" rows="6" cols="50"></textarea>
                    </label>
                </div>
            </div>
            <div class="columns large-12 medium-12 small-12">
                <button class="buttonCustomSuccess button success enviarAgregarListar" id="agregarListaNegra" type="button" name="agregarListaNegra">Agregar</button>
            </div>
        </div>-->

        <input type="text" id="myInput" onkeyup="funcionBuscar()" placeholder="Busqueda de persona.">

        <table class="responsive">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Teléfono</th>
                    <th>Comentario</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>

                @for ($i = 0; $i
                < count($lista); $i++) <tr>
                    <td>{{$lista[$i]->nombre}}</td>
                    <td>{{$lista[$i]->telefono}}</td>
                    <td>{{$lista[$i]->comentario}}</td>
                    <td>
                        <i class="fa fa-edit" onclick="modificarListaNegra(this,{{$lista[$i]->id}});"></i>
                        <i class="fas fa-trash-alt" onclick="eliminarListaNegra(this,{{$lista[$i]->id}});"></i>
                        <input type="hidden" name="idTr{{$lista[$i]->id}}" />
                    </td>
                    </tr>
                    @endfor

            </tbody>
        </table>

    </div>
</div>
<input type="hidden" id="hiddenModificar" value="" />

<div class="reveal tiny" id="modalListaN" data-reveal>
    <h3>Modificar un registro</h3>
    <div class="columns large-12 medium-12 small-12" id="divContenidoModal">

        <div class="columns row">
            <label class="labelNameInput">Nombre
                <input id="nombre" type="text" placeholder="Nombre">
            </label>
        </div>

        <div class="columns row">
            <label class="labelNameInput">Teléfono
                <input id="tel" type="text" placeholder="099999888">
            </label>
        </div>
        <div class="columns row">
            <label class="labelNameInput">Comentario
                <textarea name="comentario" id="comentario" rows="6" cols="50"></textarea>
            </label>
        </div>
        <div class="columns row">
            <button class="buttonCustomSuccess button success enviarAgregarListar" id="guardarListaNegra" type="button">Actualziar</button>
        </div>
    </div>
   
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

@include('layouts.footer_panel_resp_table')