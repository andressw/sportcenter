@include('layouts.head_login_signup')
<form class="logiForm" role="form" method="POST" action="{{ url('/login') }}">
  {{ csrf_field() }}
<div class="large-12 columns no-padding contenidoFondoLoginSigup">
  <div class="large-4 medium-4 medium-offset-4 large-offset-4 no-padding columns contenidoLoginSigup">
    <div class="large-12 columns headerDivLogin">
      <h2>HTTP Response Monitor</h2>
      <span>Login Now</span>
    </div>
    <div class="larege-12 columns contentCenter">
      @if ($errors->has('email') || $errors->has('password'))
          <div class="ErrorBox"><b>Incorrect Login Details</b> - Sorry we could not log you in. Please try again!</div>
      @endif
      <input id="email" name="email" class="email_pass" type="email" placeholder="somebody@example.com" value="{{ old('email') }}" required autofocus>
      <input id="password" name="password" class="email_pass" type="password" placeholder="Password">

      <input type="submit" class="button loginBtn" value="Log in"></input>
      <a class="link_pss_acc" href="/signup">Don't have an account?</a> -
      <a class="link_pss_acc" href="/passwordreset">Forgot Your Password?</a>


    </div>
  </div>


</div>
</form>


@include('layouts.footer_login_signup')
