
 @include('layouts.head_panel_reserva')
<div class="contentCenter columns large-10 large-offset-2 medium-12 small-12">
  <div class="contentFormulario columns large-12 medium-12 small-12">
    @if (count($canchas) > 0)
    <div id="containerCanchas" class="row align-center">
      @for ($i = 0; $i
      < count($canchas); $i++) <div class="columns large-2 medium-3 small-6 itemCancha">
        <span {{($canchas[$i]->id==$idCancha )? 'class=selected': ''}} onclick="cambiarCancha(this,{{$canchas[$i]->id}},0);">{{$canchas[$i]->nombre}}</span>
    </div>
    @endfor
</div>
<div id="containerFechaHistorial" class="row align-center">
    <div class="columns large-3 medium-4 small-12">
    <div class="input-group">
          <span class="input-group-label">
            <i class="far fa-calendar-alt"></i>
          </span>
          <input type="text" class="span2 dpFechas" value="{{$fecha}}" id="dpFechaHistorial" readonly>
        </div>
  </div>
</div>

<div class="contentReservas columns large-12 medium-12 small-12">
  <div class="columns large-12 medium-12 small-12 borderTop"></div>
  <h4 id="canchaDia">{{$canchaDia}}
    <small>({{$fechaDiaMes[2]. "/" .$fechaDiaMes[1]}})</small>
    </h4>
    <div class="referenciaColores">
      <span>Solo por el día</span>
      <div class="colorSoloDia"></div>
      <span>Cumpleaños</span>
      <div class="colorCumple"></div>
    </div>
    <input type="text" id="myInput" onkeyup="funcionBuscar()" placeholder="Busqueda de reserva.">

    <table class="responsive">
      <thead>
        <tr>
          <th>Hora</th>
          <th>Nombre</th>
          <th>Teléfono</th>
          <th>Comentario</th>
          <th>¿Jugó?</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>

        @for ($i = 0; $i
        < count($reservas); $i++) <tr {{($reservas[$i]->soloHoy =!null && $reservas[$i]->soloHoy =! '' && $reservas[$i]->soloHoy == 1)?'class=soloDia':''}}>
          <td>{{substr($reservas[$i]->hora, 0, -3)}}</td>
          <td>{{$reservas[$i]->nombre}}</td>
          <td>{{$reservas[$i]->telefono}}</td>
          <td>{{$reservas[$i]->comentario}}</td>

           @if ($reservas[$i]->idReserva != null) 
          <td>{{($reservas[$i]->jugado==1)?"Si":"No"}}</td>
          <td>
           <i class="fas fa-trash-alt" onclick="eliminarHistorial({{$reservas[$i]->idHora}},{{$reservas[$i]->idH}});"></i>
            <input type="hidden" name="idReserva{{$reservas[$i]->idReserva}}" />
            <input type="hidden" name="idTr{{$reservas[$i]->idHora}}" />
            </td>
           @else
           <td></td>
           <td><input type="hidden" name="idTr{{$reservas[$i]->idHora}}" /></td>
           @endif
          
          </tr>
          @endfor

      </tbody>
    </table>
    <input type="hidden" id="idCanchaSelected" value="{{$canchas[0]->id}}" />   
</div>
@else
<div class="noRegistrosIngresados columns large-12 medium-12 small-12">
  <span>No se encontro ninguna cancha ingresada.</span>
</div>
@endif


</div>
</div>

@include('layouts.footer_panel_reserva')
