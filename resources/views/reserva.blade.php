 @include('layouts.head_panel_reserva')
<div class="contentCenter columns large-10 large-offset-2 medium-12 small-12">
  <div class="contentFormulario columns large-12 medium-12 small-12">
    @if (count($canchas) > 0)
    <div id="containerCanchas" class="row align-center">
      @for ($i = 0; $i
      < count($canchas); $i++) <div class="columns large-2 medium-3 small-6 itemCancha">
        <span {{($canchas[$i]->id==$idCancha )? 'class=selected': ''}} onclick="cambiarCancha(this,{{$canchas[$i]->id}},1);">{{$canchas[$i]->nombre}}</span>
    </div>
    @endfor

  </div>
  <div id="containerDias" class="row align-center">
    @for ($i = 0; $i
    < count($dias); $i++) <div class="columns large-2 medium-2 small-6 itemDia">
      <span {{($dias[$i]->id == date('N'))?'class=selected':''}} onclick="cambiarDia(this,{{$dias[$i]->id}});">{{$dias[$i]->nombre}}</span>
  </div>
  @endfor
</div>
<div class="contentReservas columns large-12 medium-12 small-12">
  <div class="columns large-12 medium-12 small-12 borderTop"></div>
  <h4 id="canchaDia">{{$canchaDia}}
    <small>({{$fechaDiaMes[2]. "/" .$fechaDiaMes[1]}})</small>
    </h4>
    <div class="referenciaColores">
      <span>Solo por el día</span>
      <div class="colorSoloDia"></div>
      <span>Cumpleaños</span>
      <div class="colorCumple"></div>
    </div>
    <input type="text" id="myInput" onkeyup="funcionBuscar()" placeholder="Busqueda de reserva.">

    <table class="responsive">
      <thead>
        <tr>
          <th>Hora</th> 
          <th>Nombre</th>
          <th>Teléfono</th>
          <th>Comentario</th>
          <th>¿Jugó?</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>

        @for ($i = 0; $i
        < count($reservas); $i++) <tr {{($reservas[$i]->soloHoy =!null && $reservas[$i]->soloHoy =! '' && $reservas[$i]->soloHoy == 1)?'class=soloDia':''}}>
          <td>{{substr($reservas[$i]->hora, 0, -3)}}</td>
          <td>{{$reservas[$i]->nombre}}</td>
          <td>{{$reservas[$i]->telefono}}</td>
          <td>{{$reservas[$i]->comentario}}</td>
            @if ($reservas[$i]->idReserva == null)
            <td></td>
          <td>
            <i class="fa fa-plus-circle" onclick="agregarReserva(this,{{$reservas[$i]->idHora}},{{$idCancha}});"></i>
          
            @else
          <td><input id="jugo{{$reservas[$i]->idReserva}}" type="checkbox" {{($reservas[$i]->jugado==1)?"checked":""}} onclick="marcarComoJugado(this, {{$reservas[$i]->idReserva}});"><label for="jugo{{$reservas[$i]->idReserva}}" class="labelJugo">¿Jugo?</label></td>
          <td>
            <i class="fa fa-edit" onclick="modificarReserva({{$reservas[$i]->idHora}},{{$reservas[$i]->idReserva}});"></i>
            <i class="fas fa-trash-alt" onclick="eliminarReserva({{$reservas[$i]->idHora}},{{$reservas[$i]->idReserva}});"></i>
            <input type="hidden" name="idReserva{{$reservas[$i]->idReserva}}" /> @endif
            <input type="hidden" name="idTr{{$reservas[$i]->idHora}}" />
            </td>
          </tr>
          @endfor

      </tbody>
    </table>
    <input type="hidden" id="idCanchaSelected" value="{{$canchas[0]->id}}" />
    <input type="hidden" id="idDiaSelected" value="{{date('N')}}" />
    <input type="hidden" id="fechaSemanaSelected" value="{{$fecha}}" />
    <input type="hidden" id="hiddenModificar" value="-1" />
    <input type="hidden" id="hiddenModificarHora" value="-1" />
    
</div>
@else
<div class="noRegistrosIngresados columns large-12 medium-12 small-12">
  <span>No se encontro ninguna cancha ingresada.</span>
</div>
@endif

<div class="reveal tiny" id="modalReserva" data-reveal>
  <h3>Reserva</h3>
  <div id="contenidoModal" class="row">

    <div class="columns large-12 medium-12 small-12">

      <div class="row align-center divSoloHoySemenal">
        <button class="button primary soloDia" onclick="cargarSoloPorDiaSemanal(1);" type="button">Solo por el día</button>
        <button class="button secondary semanal" onclick="cargarSoloPorDiaSemanal(0);" type="button">Semanal</button>
      </div>

      <div class="columns row">
        <label class="labelNameInput">Cancha <span class="requerido">*</span>
          <select id="idCanchaSelect">

            @for ($i = 0; $i
            < count($canchas); $i++) <option value="{{$canchas[$i]->id}}" {{($canchas[$i]->id==$idCancha )? 'selected': ''}}>{{$canchas[$i]->nombre}}</option>
              @endfor

          </select>
        </label>
      </div>
      <label class="labelDpFecha">Fecha de la reserva <span class="requerido">*</span>
        <div class="input-group">
          <span class="input-group-label">
            <i class="far fa-calendar-alt"></i>
          </span>
          <input type="text" class="span2 dpFechas" value="{{$fecha}}" id="dpFecha" readonly>
        </div>
      </label>
      

 <label id="contentFechaDesdeHasta" class="labelDpFecha" >Fecha Desde/Hasta no juega <span data-tooltip aria-haspopup="true" class="has-tip" title="En estos 2 campos se especifica una fecha Desde/Hasta el cliente no va a jugar en la cancha, dejando la hora libre para otra reserva. Si al menos 1 de estos 2 campos estan vacíos no se tomara esta acción."><i id="ayudaFechaDesdeHasta" class="fas fa-question-circle"></i></span>
        <div class="input-group">
          <span class="input-group-label">
            <i class="far fa-calendar-alt"></i>
          </span>
          <input type="text" class="span2 dpFechas" value="" id="dpFechaDesde" readonly>
          <span class="input-group-label separadorFechas">
            <i class="fas fa-ellipsis-v"></i>
          </span>
          <input type="text" class="span2 dpFechas" value="" id="dpFechaHasta" readonly>
        </div>
      </label>

      <div class="columns row">
        <label class="labelNameInput">Hora <span class="requerido">*</span>
          <select id="horasReserva">

            @for ($i = 0; $i
            < count($reservas); $i++) <option value="{{$reservas[$i]->idHora}}" {{($reservas[$i]->idReserva != NULL)?"disabled":""}}>{{substr($reservas[$i]->hora, 0, -3)}}{{($reservas[$i]->idReserva != NULL && $reservas[$i]->soloHoy ==1)?"
              (por día)":""}}{{($reservas[$i]->idReserva != NULL && $reservas[$i]->soloHoy ==0)?"
              (semanal)":""}}</option>
              @endfor

          </select>
        </label>
      </div>

      <div class="columns row">
        <label class="labelNameInput">Nombre <span class="requerido">*</span>
          <input id="nombreReserva" type="text" placeholder="Nombre">
        </label>
      </div>


      <div class="columns row"> 
        <label class="labelNameInput">Teléfono <span class="requerido">*</span>
          <input id="telReserva" type="text" placeholder="099854695">
        </label>
      </div>
    </div>
    <div class="columns large-12 medium-12 small-12">
      <div class="columns row">
        <label class="labelNameInput">Comentario
          <textarea name="comentarioReserva" id="comentarioReserva" rows="3" cols="50"></textarea>
        </label>
      </div>
    </div>
    <div class="columns large-12 medium-12 small-12">
      <button class="buttonCustomSuccess button success enviarAgregarListar buttonReserva" id="guardarReservaSoloDia" type="button">Ingresar</button>
    </div>
    
  </div>
  
  <input type="hidden" id="hiddenReserva" value="" />
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
</div>
</div>

@include('layouts.footer_panel_reserva')