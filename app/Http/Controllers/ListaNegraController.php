<?php

namespace App\Http\Controllers;

use App\ListaNegra;
use Illuminate\Http\Request;
use DB;
use Session;

class ListaNegraController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }
    
    public function cargarVista()
    {
        /*$art = DB::table('articulos')
        ->join('categorias', 'categorias.id', '=', 'articulos.idCategoria')
        ->select('articulos.*', 'categorias.titulo as tituloC')
        ->where('articulos.estado', 1)
        ->orderby('articulos.updated_at', 'desc')
        ->get();

        $artBorradorr = DB::table('articulos')
        ->where('articulos.estado', 0)->count();
        $conf = DB::table('configuracion')->get();*/
        $lista = DB::table('lista_negras')->orderby("nombre")->get();
        return view('lista_negra', ['seccion' => 'listaNegra', "tituloSección" => 'Lista Negra', 'lista' =>$lista]);
    }

    public function getRegistro(Request $request)
    {
        /*$art = DB::table('articulos')
        ->join('categorias', 'categorias.id', '=', 'articulos.idCategoria')
        ->select('articulos.*', 'categorias.titulo as tituloC')
        ->where('articulos.estado', 1)
        ->orderby('articulos.updated_at', 'desc')
        ->get();

        $artBorradorr = DB::table('articulos')
        ->where('articulos.estado', 0)->count();
        $conf = DB::table('configuracion')->get();*/
        $lista = DB::table('lista_negras')->where('id', $request->id)->get();
        return response(["data"=> $lista], 200)
            ->header('Content-Type', 'json');
    }

    
    public function modificar(Request $request)
    {
        $this->validate($request, ["nombre" => 'required|min:1',
        "id" => 'required|min:1'
        ]);

        DB::table('lista_negras')
            ->where('id', $request->id)
            ->update([
                'nombre' => $request->nombre,
                'telefono' => ($request->tel == "")?" - ":str_replace(" ","",$request->tel),
                'comentario' => ($request->comentario == "")?" - ":$request->comentario
                ]);
        return response('Registro modificado correctamente.', 200)
            ->header('Content-Type', 'text/plain');
    }
    public function eliminar(Request $request)
    {
        
        /*
        Hacer el buvcle apra que elimine todas las reservas
        */

        DB::table('lista_negras')
            ->where('id', $request->id)
            ->delete();
        return response('Registro eliminado correctamente.', 200)
            ->header('Content-Type', 'text/plain');
    }

    public function ingresar(Request $request)
    {
        $this->validate($request, ["nombre" => 'required|min:1'
        ]);
        $tel = ($request->tel == "")?" - ":str_replace(" ","",$request->tel);
        $comentario = ($request->comentario == "")?" - ":$request->comentario;

        $lista = new ListaNegra();
        $lista->nombre = $request->nombre;
        $lista->telefono = $tel;
        $lista->comentario = $comentario;
        

        if (!$lista->save()) {
            return response('Error al ingresar el registro, intentelo nuevamente', 500)
                ->header('Content-Type', 'text/plain');
        } else {
            
            return response(["mensaje" => "Registro ingresado correctamente.", "id" => DB::table('lista_negras')->select('id')->orderBy('created_at', 'desc')->first()], 200)
                ->header('Content-Type', 'json');
        }

    }    

    

}
