<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historial extends Model
{
    /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
    protected $fillable = [
        'idHora','idReserva','idDia', 'idCancha', 'nombre', 'telefono', 'comentario', 'fecha', 'jugado', 'soloHoy'
      ];
    
}
