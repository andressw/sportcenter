@include('layouts.head_login_signup')
<form class="logiForm" role="form" method="POST" action="{{ url('/login') }}">
  {{ csrf_field() }}
<div class="row no-padding contenidoFondoLoginSigup align-middle">
  <div class="large-4 medium-6 medium-offset-3 large-offset-4 small-offset-1 small-10 no-padding columns contenidoLoginSigup">
    <div class="large-12 columns headerDivLogin">
      <h2>Sportcenter</h2>
      <span>Iniciar Sesión</span>
    </div>
    <div class="larege-12 columns contentCenter">
      @if ($errors->has('email') || $errors->has('password'))
          <div class="ErrorBox"><b>Credenciales incorrectas, intentelo de nuevo.</div>
      @endif
      <input id="email" name="email" class="email_pass" type="text" placeholder="Usuario" value="{{ old('email') }}" required autofocus>
      <input id="password" name="password" class="email_pass" type="password" placeholder="Contraseña">
      <input type="submit" class="button loginBtn" value="Ingresar">
    </div>
  </div>


</div>
</form>


@include('layouts.footer_login_signup')
