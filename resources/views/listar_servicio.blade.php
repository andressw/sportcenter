@include('layouts.head_panel')

<div class="contentCenter columns large-10 large-offset-2 medium-12 small-12">
  <div class="contentFormulario columns large-7 medium-12 small-12">
    @if (count($servicios) > 0)
    <div class="tituloTablaTr divComoTr table-expand-row columns large-12 medium-12 small-12">Nombre</div>
    @for ($i = 0; $i
    < count($servicios); $i++) <div id="trArriba{{$servicios[$i]->id}}" class="divComoTr table-expand-row columns large-12 medium-12 small-12"
      onclick="expandirOcultarContenido(this);">
      <span id="nombreId{{$servicios[$i]->id}}">{{$servicios[$i]->nombre}}</span>
      <span class="colapseExpandIcon">+</span>
  </div>


  <div id="trAbajo{{$servicios[$i]->id}}" class="divAbajoClass divComoTr table-expand-row-content columns large-12 medium-12 small-12">
    <div class="row rowModificarEliminar">
      <div class="columns large-6 medium-6 small-12 no-padding-left">
        <label class="labelNameInput">Nombre
          <input id="nombreServicio{{$servicios[$i]->id}}" type="text" placeholder="Nombre" value="{{$servicios[$i]->nombre}}">
        </label>
      </div>
      <div class="columns large-6 medium-6 small-12 no-padding-rigth">

          <label class="labelNameInput">Tipo de servicio
            <select id="selectTipoS{{$servicios[$i]->id}}">
              @for ($j = 0; $j
              < count($tipoS); $j++) <option {{($servicios[$i]->tipoServicio == $tipoS[$j]->id)?'selected':''}} value="{{$tipoS[$j]->id}}">{{$tipoS[$j]->nombre}}</option>
                @endfor
            </select>
          </label>
        </div>
        <div class="columns large-12 medium-12 small-12 no-padding">
        <div class="columns large-6 medium-6 small-12 no-padding-left">

          <label class="labelNameInput">Precio
            <input id="precioServicio{{$servicios[$i]->id}}" type="number" placeholder="Precio" value="{{$servicios[$i]->precio}}">
          </label>
          </div></div>
        <div class="divContentSuccess columns large-6 medium-6 small-6">
        <button class="enviarAgregarListar buttonCustomSuccess button success" type="button" onclick="guardarModificarServicio({{$servicios[$i]->id}},this);">Guardar</button>
      </div>
      <div class="divContentAlert columns large-6 medium-6 small-6">
        <button class="enviarAgregarListar button alert" type="button" onclick="eliminarServicio({{$servicios[$i]->id}},this);">Eliminar</button>
      </div>

      
    </div>
  </div>
  

  @endfor
  @else
  <div class="noRegistrosIngresados columns large-12 medium-12 small-12">
    <span>No se encontraron registros ingresados.</span>
  </div>
  @endif
</div>
</div>
@include('layouts.footer_panel')