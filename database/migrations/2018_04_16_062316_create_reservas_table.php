<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('idHora');
          $table->integer('idDia');
          $table->integer('idCancha');
          $table->string('nombre');
          $table->string('telefono')->nullable();
          $table->string('comentario')->nullable();
          $table->date('fecha')->nullable();
          $table->date('fechaDesdeNoJuega')->nullable();
          $table->date('fechaHastaNoJuega')->nullable();
          $table->boolean('jugado');
          $table->boolean('soloHoy');
          $table->boolean('hoyJuega');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
