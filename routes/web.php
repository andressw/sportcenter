<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');



Route::get('/admin', 'AdminController@ingresar');





Route::get('/registrar', function(){
  return view('auth/register');
});

/*Agregado, modificado y eliminado de tipos*/
Route::get('/admin/tipo-cancha/agregar', 'TiposController@cargarVistaAgregarTipoC');
Route::get('/admin/tipo-cancha/listar', 'TiposController@cargarVistaListarTipoC');

Route::get('/admin/tipo-pasto/agregar', 'TiposController@cargarVistaAgregarTipoP');
Route::get('/admin/tipo-pasto/listar', 'TiposController@cargarVistaListarTipoP');

Route::get('/admin/tipo-servicio/agregar', 'TiposController@cargarVistaAgregarTipoS');
Route::get('/admin/tipo-servicio/listar', 'TiposController@cargarVistaListarTipoS');

Route::post('/admin/agregarTCancha', 'TiposController@ingresarTC');
Route::post('/admin/agregarTPasto', 'TiposController@ingresarTP');
Route::post('/admin/agregarTServicio', 'TiposController@ingresarTS');

Route::post('/admin/guardarModificarTipo', 'TiposController@modificarTipos');
Route::post('/admin/eliminarTipo', 'TiposController@eliminarTipos');



/*----------------------------------------*/

/*Demas rutas*/
Route::get('/admin/lista-negra', 'ListaNegraController@cargarVista');

Route::get('/admin/servicio/listar', 'ServicioController@cargarVistaListarServicio');
Route::get('/admin/servicio/agregar', 'ServicioController@cargarVistaAgregarServicio');

Route::get('/admin/cancha/listar', 'CanchaController@cargarVistaListarCancha');
Route::get('/admin/cancha/agregar', 'CanchaController@cargarVistaAgregarCancha');

Route::get('/admin/historial-reservas', 'HistorialController@cargaarVistaHistorial');

Route::get('/admin/multimedia', 'MultimediaController@cargarVistaMultimedia');

Route::get('/admin/reservas', 'ReservaController@cargarWeb');
/*-------------------------------------------*/

/*-------------------Agregar-Modificar-Eliminar-Cancha----------------- */
Route::post('/admin/agregarCancha', 'CanchaController@ingresarCancha');
Route::post('/admin/modificarCancha', 'CanchaController@modificarCancha');
Route::post('/admin/eliminarCancha', 'CanchaController@eliminarCancha');

/*--------------------------------------------------------------------- */

/*-------------------Agregar-Modificar-Eliminar-servicio----------------- */
Route::post('/admin/agregarServicio', 'ServicioController@ingresarServicio');
Route::post('/admin/modificarServicio', 'ServicioController@modificarServicio');
Route::post('/admin/eliminarServicio', 'ServicioController@eliminarServicio');

/*--------------------------------------------------------------------- */

/*-------------------Agregar-Modificar-Eliminar-lista-negra----------------- */
Route::post('/admin/agregarListaNegra', 'ListaNegraController@ingresar');
Route::post('/admin/modificarListaNegra', 'ListaNegraController@modificar');
Route::post('/admin/eliminarListaNegra', 'ListaNegraController@eliminar');
Route::post('/admin/getRegistroModificarListaNegra', 'ListaNegraController@getRegistro');
/*--------------------------------------------------------------------- */

/*-------------------Agregar-Modificar-Eliminar-reserva----------------- */
Route::post('/admin/agregarReservaSoloDia', 'ReservaController@agregarSoloPorDia');
Route::post('/admin/actualizarReservaSoloDia', 'ReservaController@actualizarReservaSoloDia');

Route::post('/admin/cargarReservasCancha', 'ReservaController@cargarReservasCancha');
Route::post('/admin/darHorasFecha', 'ReservaController@darHorasFecha');
Route::post('/admin/corroborarFechaSoloDiaParaSemanal', 'ReservaController@corroborarFechaSoloDiaParaSemanal');
Route::post('/admin/eliminarReserva', 'ReservaController@eliminarReserva');
Route::post('/admin/getRegistroModificarReserva', 'ReservaController@getRegistroModificarReserva');
Route::post('/admin/chequearPersonaEnListaNegra', 'ReservaController@chequearPersonaEnListaNegra');
Route::post('/admin/cambiarJugoReserva', 'ReservaController@cambiarJugoReserva');


/*Historial*/

Route::post('/admin/cargarHistoralCancha', 'HistorialController@cargarHistoralCancha');
Route::post('/admin/eliminarHistorial', 'HistorialController@eliminarHistorial');

/*Cumpleaños*/
Route::get('/admin/cumpleanos', 'CumpleController@cargarWeb');



/*Route::post('/admin/modificarListaNegra', 'ListaNegraController@modificar');
Route::post('/admin/eliminarListaNegra', 'ListaNegraController@eliminar');
Route::post('/admin/getRegistroModificarListaNegra', 'ListaNegraController@getRegistro');*/
/*--------------------------------------------------------------------- */

