module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      options: {
        loadPath: ['bower_components/foundation-sites/scss']
      },
      dist: {
        options: {
          style: 'compressed'
        },
        files: {
          'public/css/app.css': 'resources/assets/sass/app.scss'
        }
      },

      app: {
        options: {
          style: 'compressed'
        },
        files: [{
          expand: true,
          cwd: 'resources/assets/sass/',
          src: ['**/*.scss'],
          dest: 'public/css/',
          ext: '.css'
        }]
      }
    },

    copy: {
      scripts: {
        expand: true,
        cwd: 'bower_components/foundation-sites/dist/js/',
        src: '**/*.js',
        dest: 'public/js'
      }
    },

    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: [
          'resources/assets/js/jquery.min.js',
          /*'public/js/responsive-table/javascripts/jquery.min.js',*/
          'resources/assets/js/jquery-ui.js',
          'bower_components/foundation-sites/dist/js/foundation.js',
          'bower_components/what-input/dist/what-input.js',
          'resources/assets/js/app.js'
        ],

        dest: 'public/js/app.js'
      }
    },

    uglify: {
      options: {
        // Grunt can replace variables names, but may not be a good idea for you, I leave
        // this option as false
        mangle: false
      },
      js: {
        // Grunt will search for "**/*.js" when the "minify" task
        // runs and build the appropriate src-dest file mappings then, so you
        // don't need to update the Gruntfile when files are added or removed.
        files: [{
          expand: true,
          cwd: 'public/js/',
          src: 'app.js',
          dest: 'public/js/',
          ext: '.min.js',
        }]
      }
    },

  /*  uglify: {
      dist: {
        files: {
          'public/js/app.min.js': ['public/js/app.js']
        }
      },

      all: {
        files: [{
          expand: true,
          cwd: 'resources/assets/js/',
          src: 'app.js',
          dest: 'public/js/',
          ext: '.min.js',
        }]
      }
    },*/
    watch: {
      css: {
        files: ['resources/assets/sass/*.scss','resources/assets/sass/otros/*.scss'],
        tasks: ['sass']
      },
      js: {
        files: ['resources/assets/js/*.js'],
        tasks: ['concat', 'uglify']
      }
    }

  });

  // Task definition
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['sass', 'concat', 'uglify']);

};
