<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin Sportcenter</title>

  <link href="/css/app.css" rel="stylesheet"/>
  <link rel="stylesheet" href="/font/css/all.css">
  <style>
  @import url('https://fonts.googleapis.com/css?family=Muli|Work+Sans');
  </style>
</head>
<body>

  <div class="off-canvas position-left" id="offCanvas" data-off-canvas>
    <h2 class="tituloMenu">Sport Center</h2>
    <ul class="menu vertical accordion-menu menuPrincipal" data-accordion-menu>
      <li class="{{($seccion =='reservas')?'menuActive':''}}"><a href="/admin/reservas">Reservas</a></li>
      <li class="{{($seccion =='cumple')?'menuActive':''}}"><a href="/admin/cumpleanos">Cumpleaños</a></li>
      <li class="{{($seccion =='historialReservas')?'menuActive':''}}"><a href="/admin/historial-reservas">Historial de Reservas</a></li>

      
      <li class="liPrincipal"><a href="#">Canchas</a>
        <ul class="menu vertical nested menuHijo">
          <li class="{{($seccion =='agregarCancha')?'menuActive':''}}"><a href="/admin/cancha/agregar">Agregar</a></li>
          <li class="{{($seccion =='listarCancha')?'menuActive':''}}"><a href="/admin/cancha/listar">Listar todas</a></li>
        </ul>
      </li>
      <li class="liPrincipal"><a href="#">Servicios</a>
        <ul class="menu vertical nested menuHijo">
          <li class="{{($seccion =='agregarServicio')?'menuActive':''}}"><a href="/admin/servicio/agregar">Agregar</a></li>
          <li class="{{($seccion =='listarServicio')?'menuActive':''}}"><a href="/admin/servicio/listar">Listar todas</a></li>
        </ul>
      </li>
      <li class="{{($seccion =='listaNegra')?'menuActive':''}}"><a href="/admin/lista-negra">Lista Negra</a></li>

      <li class="liPrincipal"><a href="#">Tipo Cancha</a>
        <ul id="ulTipoC" class="menu vertical nested menuHijo">
          <li class="{{($seccion =='agregarTipoC')?'menuActive':''}}"><a href="/admin/tipo-cancha/agregar">Agregar</a></li>
          <li class="{{($seccion =='listarTipoC')?'menuActive':''}}"><a href="/admin/tipo-cancha/listar">Listar todas</a></li>
        </ul>
      </li>

      <li class="liPrincipal"><a href="#">Tipo Pasto</a>
        <ul class="menu vertical nested menuHijo">
          <li class="{{($seccion =='agregarTipoP')?'menuActive':''}}"><a href="/admin/tipo-pasto/agregar">Agregar</a></li>
          <li class="{{($seccion =='listarTipoP')?'menuActive':''}}"><a href="/admin/tipo-pasto/listar">Listar todas</a></li>
        </ul>
      </li>

      <li class="liPrincipal"><a href="#">Tipo Servicio</a>
        <ul class="menu vertical nested menuHijo">
          <li class="{{($seccion =='agregarTipoS')?'menuActive':''}}"><a href="/admin/tipo-servicio/agregar">Agregar</a></li>
          <li class="{{($seccion =='listarTipoS')?'menuActive':''}}"><a href="/admin/tipo-servicio/listar">Listar</a></li>
        </ul>
      </li>

      <li class="{{($seccion =='multimedia')?'menuActive':''}}"><a href="/admin/multimedia">Multimedia</a></li>

      <li><a href="{{ url('/logout') }}"
        onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
        Salir
      </a>

      <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
      </form></li>

    </ul>
  </div>

  <div class="off-canvas-content" data-off-canvas-content>
    <div class="title-bar ocultarEnPc">
      <div class="title-bar-left">
        <button class="left-off-canvas-toggle menu-icon" type="button" data-open="offCanvas"></button>
        <span class="title-bar-title">Sport Center</span>
      </div>
    </div>
    <div class="contentTitulo columns large-12 medium-12 small-12">
  <div class="columns large-10 large-offset-2 medium-12 small-12">
    <h3>{{$tituloSección}}</h3>
  </div>
</div>
