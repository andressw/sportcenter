<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Admin Sportcenter</title>

  <link href="/css/app.css" rel="stylesheet"/>
  <link rel="stylesheet" href="/font/css/all.css">
  <style>
@import url('https://fonts.googleapis.com/css?family=Muli|Work+Sans');
</style>
<!-- Scripts -->
<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>
</head>
<body class="bodyLoginSignup">
