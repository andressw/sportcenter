@include('layouts.head_panel')


<div class="contentCenter columns large-10 large-offset-2 medium-12 small-12">
  <div class="contentFormulario columns large-7 medium-12 small-12">
  @if ((count($tipoC) > 0) && (count($tipoP) > 0))
  <label class="labelNameInput" for="nombreC">Nombre
      <input id="nombreC" type="text" name="nombreC" placeholder="Ej: Cancha Nueva">
    </label>

    <label class="labelNameInput">Tipo de cancha
    <select id="selectTipoC">
    @for ($i = 0; $i < count($tipoC); $i++)
    <option value="{{$tipoC[$i]->id}}">{{$tipoC[$i]->nombre}}</option>
    @endfor
  </select>
    </label>

    <label class="labelNameInput">Tipo de pasto
    <select id="selectTipoP">
    @for ($i = 0; $i < count($tipoP); $i++)
    <option value="{{$tipoP[$i]->id}}">{{$tipoP[$i]->nombre}}</option>
    @endfor
  </select>
    </label>

    <label class="labelNameInput">Permite cumpleaños
    <select id="permiteCumple">
      <option value="0">No</option>
    <option value="1">Si</option>
  </select>
    </label>

    <label class="labelNameInput" for="precioC">Precio
      <input id="precioC" type="number" name="precioC" placeholder="Ej: 300">
    </label>
    <!--<label class="labelNameInput" for="precioC">Imagen
    <form id="formImageSerealize" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="file" id="fileImagen" name="fileImagen">
      </form>
    </label>-->

    <button class="buttonCustomSuccess button success enviarAgregarListar" id="agregarCancha" type="button" name="agregarCancha">Agregar</button>
@else
<div class="noRegistrosIngresados columns large-12 medium-12 small-12">
      <span>Deben existir un tipo de cancha y un tipo de pasto ingresados.</span>
    </div>
@endif
  </div>

</div>
@include('layouts.footer_panel')