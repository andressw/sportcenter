<?php

namespace App\Http\Controllers;

use App\TipoCancha;
use App\TipoPasto;
use App\TipoServicio;
use DB;
use Illuminate\Http\Request;

class TiposController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function cargarVistaAgregarTipoC()
    {
        /*$art = DB::table('articulos')
        ->join('categorias', 'categorias.id', '=', 'articulos.idCategoria')
        ->select('articulos.*', 'categorias.titulo as tituloC')
        ->where('articulos.estado', 1)
        ->orderby('articulos.updated_at', 'desc')
        ->get();

        $artBorradorr = DB::table('articulos')
        ->where('articulos.estado', 0)->count();
        $conf = DB::table('configuracion')->get();*/
        return view('tipo_cancha_agregar', ['seccion' => 'agregarTipoC', "tituloSección" => 'Agregar tipo de cancha']);
    }

    public function cargarVistaListarTipoC()
    {
        $tipos = DB::table('tipo_canchas')->get();
        return view('tipo_cancha_listar', ['seccion' => 'listarTipoC', "tituloSección" => 'Listar tipos de canchas', "tipos" => $tipos]);
    }

    public function cargarVistaAgregarTipoP()
    {
        return view('tipo_pasto_agregar', ['seccion' => 'agregarTipoP', "tituloSección" => 'Agregar tipo de pasto']);
    }

    public function cargarVistaListarTipoP()
    {
        $tipos = DB::table('tipo_pastos')->get();
        return view('tipo_pasto_listar', ['seccion' => 'listarTipoP', "tituloSección" => 'Listar tipos de pastos', "tipos" => $tipos]);
    }

    public function cargarVistaAgregarTipoS()
    {
        return view('tipo_servicio_agregar', ['seccion' => 'agregarTipoS', "tituloSección" => 'Agregar tipo de servicio']);
    }

    public function cargarVistaListarTipoS()
    {
        $tipos = DB::table('tipo_servicios')->get();
        return view('tipo_servicio_listar', ['seccion' => 'listarTipoS', "tituloSección" => 'Listar tipos de servicios', "tipos" => $tipos]);
    }

    public function modificarTipos(Request $request)
    {
        $tabla = '';
        switch ($request->tipo) {
            case '0':
                $tabla = 'tipo_canchas';
                break;
            case '1':
                $tabla = 'tipo_pastos';
                break;
            case '2':
                $tabla = 'tipo_servicios';
                break;
        }

        DB::table($tabla)
            ->where('id', $request->id)
            ->update(['nombre' => $request->nombre]);
        return response('Registro modificado correctamente.', 200)
            ->header('Content-Type', 'text/plain');
    }
    public function eliminarTipos(Request $request)
    {
        $tabla = '';
        switch ($request->tipo) {
            case '0':
                $tabla = 'tipo_canchas';
                $nombreTipo = "tipoCancha";
                break;
            case '1':
                $tabla = 'tipo_pastos';
                $nombreTipo = "tipoPasto";
                break;
            case '2':
                $tabla = 'tipo_servicios';
                break;
        }
        if ($request->tipo != 2) {
            $cancha = DB::table('canchas')->where($nombreTipo, $request->id)->get();
            for ($i = 0; $i < count($cancha); $i++) {
                DB::table('canchas')
                    ->where('id', $cancha[$i]->id)
                    ->delete();
                DB::table('reservas')
                    ->where('idCancha', $cancha[$i]->id)
                    ->delete();
            }
        }

        DB::table($tabla)
            ->where('id', $request->id)
            ->delete();

        return response('Registro eliminado correctamente.', 200)
            ->header('Content-Type', 'text/plain');
    }

    public function ingresarTC(Request $request)
    {
        $this->validate($request, ["nombreTC" => 'required|min:1']);

        $tipo = new TipoCancha();
        $tipo->nombre = $request->nombreTC;

        if (!$tipo->save()) {
            return response('Error al ingresar el registro, intentelo nuevamente', 500)
                ->header('Content-Type', 'text/plain');
        } else {
            return response('Tipo de Cancha ingresado correctamente.', 200)
                ->header('Content-Type', 'text/plain');
        }

    }

    public function ingresarTP(Request $request)
    {
        $this->validate($request, [
            "nombreTP" => 'required|min:1',
        ]);

        $tipo = new TipoPasto();
        $tipo->nombre = $request->nombreTP;

        if (!$tipo->save()) {
            return response('Error al ingresar el registro, intentelo nuevamente', 500)
                ->header('Content-Type', 'text/plain');
        } else {
            return response('Tipo de Pasto ingresado correctamente.', 200)
                ->header('Content-Type', 'text/plain');
        }

    }

    public function ingresarTS(Request $request)
    {
        $this->validate($request, [
            "nombreTS" => 'required|min:1',
        ]);

        $tipo = new TipoServicio();
        $tipo->nombre = $request->nombreTS;

        if (!$tipo->save()) {
            return response('Error al ingresar el registro, intentelo nuevamente', 500)
                ->header('Content-Type', 'text/plain');
        } else {
            return response('Tipo de Servicio ingresado correctamente.', 200)
                ->header('Content-Type', 'text/plain');
        }

    }

}
