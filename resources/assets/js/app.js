$(document).ready(function () {

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
    }
  });

  $('#modalReserva').on('closed.zf.reveal', function () {
    $("#dpFecha").val($("#fechaSemanaSelected").val());
    $("#idCanchaSelect").val($("#idCanchaSelected").val());
    $("#contentFechaDesdeHasta").hide();
    $("button.soloDia").removeClass("secondary").addClass("primary");
    $("button.semanal").removeClass("primary").addClass("secondary");
    $("#dpFechaDesde").val("");
    $("#dpFechaHasta ").val("");
    $("#nombreReserva").val("");
    $("#telReserva").val("");
    $("#comentarioReserva").val("");
    actualizarHorasSelect(($("button.soloDia").hasClass("primary")) ? 1 : 0);
    $("#hiddenModificar").val("-1");
    $("#hiddenModificarHora").val("-1")
  });

  $("#dpFecha").change(function () {
    actualizarHorasSelect(($("button.soloDia").hasClass("primary")) ? 1 : 0);
  });

  $("#dpFechaHistorial").change(function () {
    cargarReservasCanchaDiaHistorial();
  });
  $("#idCanchaSelect").change(function () {
    actualizarHorasSelect(($("button.soloDia").hasClass("primary")) ? 1 : 0);
  });

  $(".menuActive").parent().css("display", "block");
  if (window.location.pathname == "/admin/reservas") {
    var d = new Date();

    var month = d.getMonth() + 1;
    var monthEnd = d.getMonth() + 4;
    var day = d.getDate();
    var output = d.getFullYear() + '-' +
      (('' + month).length < 2 ? '0' : '') + month + '-' +
      (('' + day).length < 2 ? '0' : '') + day;

    var endPut = d.getFullYear() + '-' +
      (('' + monthEnd).length < 2 ? '0' : '') + monthEnd + '-' +
      (('' + day).length < 2 ? '0' : '') + day;

    $('#dpFecha').fdatepicker({
      format: 'yyyy-mm-dd',
      disableDblClickSelection: true,
      startDate: output,
      endDate: endPut,
      language: 'es'
    });

    $('#dpFechaDesde').fdatepicker({
      format: 'yyyy-mm-dd',
      disableDblClickSelection: true,
      startDate: output,
      endDate: endPut,
      language: 'es'
    });
    $('#dpFechaHasta').fdatepicker({
      format: 'yyyy-mm-dd',
      disableDblClickSelection: true,
      startDate: output,
      endDate: endPut,
      language: 'es'
    });
  } else if (window.location.pathname == "/admin/historial-reservas") {
    $('#dpFechaHistorial').fdatepicker({
      format: 'yyyy-mm-dd',
      disableDblClickSelection: true,
      language: 'es'
    });
  } else if (window.location.pathname == "/admin/cumpleanos") {
    var d = new Date();

    var month = d.getMonth() + 1;
    var monthEnd = d.getMonth() + 4;
    var day = d.getDate();
    var output = d.getFullYear() + '-' +
      (('' + month).length < 2 ? '0' : '') + month + '-' +
      (('' + day).length < 2 ? '0' : '') + day;
      
    $('#dpFechaCumple').fdatepicker({
      format: 'yyyy-mm-dd',
      disableDblClickSelection: true,
      startDate: output,
      language: 'es'
    });
  }


  $("#agregarTCancha").click(function () {
    var valor = $("#nombreTipoC").val();
    var dataEnviar = {
      nombreTC: valor
    };
    $.ajax({
      url: "/admin/agregarTCancha",
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      $("#nombreTipoC").val("");
      $('div.contentFormulario').append('<div class="successBox callout columns large-12 medium-12 small-12 success">Registro agregado correctamente</div>');
      setTimeout(function () {
        $("div.successBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 5000);
    }).fail(function (data) {
      $('div.contentFormulario').append('<div class="ErrorBox columns large-12 medium-12 small-12 callout alert">El campo nombre no puede estar vacío.</div>');
      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 5000);
    });

  });


  $("#agregarTPasto").click(function () {
    var valor = $("#nombreTipoP").val();
    var dataEnviar = {
      nombreTP: valor
    };
    $.ajax({
      url: "/admin/agregarTPasto",
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      $("#nombreTipoP").val("");
      $('div.contentFormulario').append('<div class="successBox callout columns large-12 medium-12 small-12 success">Registro agregado correctamente</div>');
      setTimeout(function () {
        $("div.successBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 5000);
    }).fail(function (data) {
      $('div.contentFormulario').append('<div class="ErrorBox columns large-12 medium-12 small-12 callout alert">El campo nombre no puede estar vacío.</div>');
      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 5000);
    });

  });


  $("#agregarTServicio").click(function () {
    var valor = $("#nombreTipoS").val();
    var dataEnviar = {
      nombreTS: valor
    };
    $.ajax({
      url: "/admin/agregarTServicio",
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      $("#nombreTipoS").val("");
      $('div.contentFormulario').append('<div class="successBox callout columns large-12 medium-12 small-12 success">Registro agregado correctamente</div>');
      setTimeout(function () {
        $("div.successBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 5000);
    }).fail(function (data) {
      $('div.contentFormulario').append('<div class="ErrorBox columns large-12 medium-12 small-12 callout alert">El campo nombre no puede estar vacío.</div>');
      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 5000);
    });
  });

  $("#agregarCancha").click(function () {
    var nombreC = $("#nombreC").val();
    var precioC = $("#precioC").val();
    var permiteCumpleC = $("#permiteCumple").val();
    var selectTipoPC = $("#selectTipoP").val();
    var selectTipoCC = $("#selectTipoC").val();


    var dataEnviar = {
      nombre: nombreC,
      precio: precioC,
      permiteCumple: permiteCumpleC,
      selectTipoP: selectTipoPC,
      selectTipoC: selectTipoCC
    };
    $.ajax({
      url: "/admin/agregarCancha",
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      $("#nombreC").val("");
      $("#precioC").val("");
      $('div.contentFormulario').append('<div class="successBox callout columns large-12 medium-12 small-12 success">' + data + '</div>');
      setTimeout(function () {
        $("div.successBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 5000);
    }).fail(function (data) {
      $('div.contentFormulario').append('<div class="ErrorBox columns large-12 medium-12 small-12 callout alert">El campo nombre y precio no pueden estar vacíos.</div>');
      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 5000);
    });

  });

  $("#agregarServicio").click(function () {
    var nombreS = $("#nombreS").val();
    var precioS = $("#precioS").val();
    var selectTipoSS = $("#selectTipoS").val();


    var dataEnviar = {
      nombre: nombreS,
      precio: precioS,
      selectTipoS: selectTipoSS
    };
    $.ajax({
      url: "/admin/agregarServicio",
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      $("#nombreS").val("");
      $("#precioS").val("");
      $('div.contentFormulario').append('<div class="successBox callout columns large-12 medium-12 small-12 success">' + data + '</div>');
      setTimeout(function () {
        $("div.successBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 5000);
    }).fail(function (data) {
      $('div.contentFormulario').append('<div class="ErrorBox columns large-12 medium-12 small-12 callout alert">El campo nombre y precio no pueden estar vacíos.</div>');
      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 5000);
    });

  });



  $("#guardarListaNegra").click(function () {
    var nombreL = $("#nombre").val();
    var telL = $("#tel").val();
    var comentarioL = $("#comentario").val();
    var idL = $("#hiddenModificar").val();
    var urlLN = "";
    if (idL == "-1") {
      urlLN = "/admin/agregarListaNegra";
    } else {
      urlLN = "/admin/modificarListaNegra";
    }


    var dataEnviar = {
      nombre: nombreL,
      tel: telL,
      comentario: comentarioL,
      id: idL
    };
    $.ajax({
      url: urlLN,
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      $("#nombre").val("");
      $("#tel").val("");
      $("#comentario").val("");

      if (idL == "-1") {
        if ($("div.pinned table").length == 1) {
          $("table.responsive").prepend('<tr><td>' + nombreL + '</td><td>' + ((telL != "") ? telL : " - ") + '</td><td>' + ((comentarioL != "") ? comentarioL : " - ") + '</td><td><i class="fa fa-edit" onclick="modificarListaNegra(this,' + data.id.id + ');"></i><i class="fas fa-trash-alt" onclick="eliminarListaNegra(this,' + data.id.id + ');"></i><input type="hidden" name="idTr' + data.id.id + '" /></td></tr>');
          $("div.pinned table").prepend('<tr style="height: 59px;"><td>' + nombreL + '</td><td style="display: none;">' + ((telL != "") ? telL : " - ") + '</td><td style="display: none;">' + ((comentarioL != "") ? comentarioL : " - ") + '</td><td style="display: none;"><i class="fa fa-edit" onclick="modificarListaNegra(this,' + data.id.id + ');"></i><i class="fas fa-trash-alt" onclick="eliminarListaNegra(this,' + data.id.id + ');"></i><input type="hidden" name="idTr' + data.id.id + '"/></td></tr>');
        } else {
          $("table.responsive").prepend('<tr><td>' + nombreL + '</td><td>' + ((telL != "") ? telL : " - ") + '</td><td>' + ((comentarioL != "") ? comentarioL : " - ") + '</td><td><i class="fa fa-edit" onclick="modificarListaNegra(this,' + data.id.id + ');"></i><i class="fas fa-trash-alt" onclick="eliminarListaNegra(this,' + data.id.id + ');"></i><input type="hidden" name="idTr' + data.id.id + '"/></td></tr>');
        }
        cambioDeColorSuccess(data.id.id);
      } else {
        $("#hiddenModificar").val("");
        $('input[name=idTr' + idL + ']').parent().parent().find("td:eq(0)").text(nombreL);
        $('input[name=idTr' + idL + ']').parent().parent().find("td:eq(1)").text((telL != "") ? telL : " - ");
        $('input[name=idTr' + idL + ']').parent().parent().find("td:eq(2)").text((comentarioL != "") ? comentarioL : " - ");
        cambioDeColorSuccess(idL);
      }
      $('#modalListaN').foundation('close');


    }).fail(function (data) {
      $("#divContenidoModal").append('<div class="columns row"><div class="ErrorBox columns large-12 medium-12 small-12 callout alert">El campo nombre no puede estar vacío.</div></div>');

      $('.reveal-overlay').animate({
          scrollTop: $('div.ErrorBox').offset().top - 80
        },
        700);
      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 5000);
    });

  });



  $("#guardarReservaSoloDia").click(function () {
    var canchaR = $("#idCanchaSelect").val();
    var dpFechaR = $("#dpFecha").val();
    var horasReservaR = $("#horasReserva").val();
    var nombreReservaR = $("#nombreReserva").val();
    var telReservaR = $("#telReserva").val();
    var comentarioReservaR = $("#comentarioReserva").val();
    var fechaDesdeR = ($("#dpFechaDesde").val() != '' && $("#dpFechaHasta").val() != '') ? $("#dpFechaDesde").val() : null;
    var fechaHastaR = ($("#dpFechaDesde").val() != '' && $("#dpFechaHasta").val() != '') ? $("#dpFechaHasta").val() : null;
    var soloDiaR = ($("button.soloDia").hasClass("primary")) ? 1 : 0;
    var hiddenModificar = $("#hiddenModificar").val();
    var urlR = (hiddenModificar == "-1") ? "/admin/agregarReservaSoloDia" : "/admin/actualizarReservaSoloDia";
    var selectedJugo = ($("#jugo" + hiddenModificar).length == 1 && $("#jugo" + hiddenModificar + ":checked").length == 1) ? "checked" : "";
    console.log(selectedJugo);

    chequearPersonaEnListaNegra(function (personaExiste) {
      if (personaExiste > 0) {
        var r = confirm("El teléfono de contacto " + telReservaR + " esta en la lista negra, ¿Desea realizar la reserva de todas formas?");
        if (r == true) {
          if (soloDiaR == 0) {
            corroborarFechaSoloDiaParaSemanal(function (resultadoPuede) {
              if (resultadoPuede.resultado == false) {

                var r = confirm(resultadoPuede.texto);
                if (r == true) {
                  var dataEnviar = {
                    cancha: canchaR,
                    dpFecha: dpFechaR,
                    horasReserva: horasReservaR,
                    nombreReserva: nombreReservaR,
                    telReserva: telReservaR,
                    comentarioReserva: comentarioReservaR,
                    fechaDesde: fechaDesdeR,
                    fechaHasta: fechaHastaR,
                    soloDia: soloDiaR,
                    eliminarReservas: 1,
                    idReservaHidden: hiddenModificar
                  };
                  $.ajax({
                    url: urlR,
                    type: 'POST',
                    data: dataEnviar
                  }).done(function (data) {
                    $("#nombreReserva").val("");
                    $("#telReserva").val("");
                    $("#comentarioReserva").val("");
                    eliminarContenidoTr($("#hiddenModificarHora").val());
                    if (data.entreLyD == true && data.id.idDia == $("#idDiaSelected").val() && canchaR == $("#idCanchaSelected").val()) {


                      agregarActualizarTr(horasReservaR, nombreReservaR, telReservaR, comentarioReservaR, data.id.id, selectedJugo);

                      cambioDeColorSuccess(horasReservaR);
                      if (soloDiaR == 1) {
                        setTimeout(function () {
                          $('input[name=idTr' + horasReservaR + ']').parent().parent().addClass("soloDia");
                        }, 1600);
                      }
                    } else {
                      $(".contentCenter").append('<div id="successReserva" class="successBox callout columns large-12 medium-12 small-12 success">Reserva agregada correctamente para la fecha <strong>' + dpFechaR + '</strong> en cancha <strong>' + $("#idCanchaSelect option:selected").text() + '</strong> </div>');
                      setTimeout(function () {
                        $("#successReserva").fadeOut(400, function () {
                          $(this).remove();
                        });
                      }, 5000);

                    }
                    actualizarHorasSelect(($("button.soloDia").hasClass("primary")) ? 1 : 0);
                    $('#modalReserva').foundation('close');
                  }).fail(function (data) {
                    var mensaje = (data.status == 500) ? "La fecha Desde debe ser menor o igual a la fecha Hasta." : 'Asegurese de haber ingresado todos los campos requeridos.';

                    $("#modalReserva").append('<div class="columns row"><div class="ErrorBox columns large-12 medium-12 small-12 callout alert">' + mensaje + '</div></div>');
                    $('.reveal-overlay').animate({
                        scrollTop: $('div.ErrorBox').offset().top - 80
                      },
                      700);


                    setTimeout(function () {
                      $("div.ErrorBox").fadeOut(400, function () {
                        $(this).remove();
                      });
                    }, 5000);
                  });
                }
              } else {
                var dataEnviar = {
                  cancha: canchaR,
                  dpFecha: dpFechaR,
                  horasReserva: horasReservaR,
                  nombreReserva: nombreReservaR,
                  telReserva: telReservaR,
                  comentarioReserva: comentarioReservaR,
                  fechaDesde: fechaDesdeR,
                  fechaHasta: fechaHastaR,
                  soloDia: soloDiaR,
                  idReservaHidden: hiddenModificar
                };
                $.ajax({
                  url: urlR,
                  type: 'POST',
                  data: dataEnviar
                }).done(function (data) {
                  $("#nombreReserva").val("");
                  $("#telReserva").val("");
                  $("#comentarioReserva").val("");
                  eliminarContenidoTr($("#hiddenModificarHora").val());
                  if (data.entreLyD == true && data.id.idDia == $("#idDiaSelected").val() && canchaR == $("#idCanchaSelected").val()) {
                    agregarActualizarTr(horasReservaR, nombreReservaR, telReservaR, comentarioReservaR, data.id.id, selectedJugo);

                    cambioDeColorSuccess(horasReservaR);
                    if (soloDiaR == 1) {
                      setTimeout(function () {
                        $('input[name=idTr' + horasReservaR + ']').parent().parent().addClass("soloDia");
                      }, 1600);
                    }
                  } else {
                    $(".contentCenter").append('<div id="successReserva" class="successBox callout columns large-12 medium-12 small-12 success">Reserva agregada correctamente para la fecha <strong>' + dpFechaR + '</strong> en cancha <strong>' + $("#idCanchaSelect option:selected").text() + '</strong></div>');
                    setTimeout(function () {
                      $("#successReserva").fadeOut(400, function () {
                        $(this).remove();
                      });
                    }, 5000);

                  }
                  actualizarHorasSelect(($("button.soloDia").hasClass("primary")) ? 1 : 0);
                  $('#modalReserva').foundation('close');
                }).fail(function (data) {
                  var mensaje = (data.status == 500) ? "La fecha Desde debe ser menor o igual a la fecha Hasta." : 'Asegurese de haber ingresado todos los campos requeridos.';

                  $("#modalReserva").append('<div class="columns row"><div class="ErrorBox columns large-12 medium-12 small-12 callout alert">' + mensaje + '</div></div>');
                  $('.reveal-overlay').animate({
                      scrollTop: $('div.ErrorBox').offset().top - 80
                    },
                    700);


                  setTimeout(function () {
                    $("div.ErrorBox").fadeOut(400, function () {
                      $(this).remove();
                    });
                  }, 5000);
                });
              }
            });


          } else {
            var dataEnviar = {
              cancha: canchaR,
              dpFecha: dpFechaR,
              horasReserva: horasReservaR,
              nombreReserva: nombreReservaR,
              telReserva: telReservaR,
              comentarioReserva: comentarioReservaR,
              fechaDesde: fechaDesdeR,
              fechaHasta: fechaHastaR,
              soloDia: soloDiaR,
              idReservaHidden: hiddenModificar
            };
            $.ajax({
              url: urlR,
              type: 'POST',
              data: dataEnviar
            }).done(function (data) {
              $("#nombreReserva").val("");
              $("#telReserva").val("");
              $("#comentarioReserva").val("");
              eliminarContenidoTr($("#hiddenModificarHora").val());
              if (data.entreLyD == true && data.id.idDia == $("#idDiaSelected").val() && canchaR == $("#idCanchaSelected").val()) {
                agregarActualizarTr(horasReservaR, nombreReservaR, telReservaR, comentarioReservaR, data.id.id, selectedJugo);

                cambioDeColorSuccess(horasReservaR);
                if (soloDiaR == 1) {
                  setTimeout(function () {
                    $('input[name=idTr' + horasReservaR + ']').parent().parent().addClass("soloDia");
                  }, 1600);
                }
              } else {
                $(".contentCenter").append('<div id="successReserva" class="successBox callout columns large-12 medium-12 small-12 success">Reserva agregada correctamente para la fecha <strong>' + dpFechaR + '</strong> en cancha <strong>' + $("#idCanchaSelect option:selected").text() + '</strong></div>');
                setTimeout(function () {
                  $("#successReserva").fadeOut(400, function () {
                    $(this).remove();
                  });
                }, 5000);

              }
              actualizarHorasSelect(($("button.soloDia").hasClass("primary")) ? 1 : 0);
              $('#modalReserva').foundation('close');
            }).fail(function (data) {
              var mensaje = (data.status == 500) ? "La fecha Desde debe ser menor o igual a la fecha Hasta." : 'Asegurese de haber ingresado todos los campos requeridos.';

              $("#modalReserva").append('<div class="columns row"><div class="ErrorBox columns large-12 medium-12 small-12 callout alert">' + mensaje + '</div></div>');
              $('.reveal-overlay').animate({
                  scrollTop: $('div.ErrorBox').offset().top - 80
                },
                700);


              setTimeout(function () {
                $("div.ErrorBox").fadeOut(400, function () {
                  $(this).remove();
                });
              }, 5000);
            });
          }
        }
      } else {
        if (soloDiaR == 0) {
          corroborarFechaSoloDiaParaSemanal(function (resultadoPuede) {
            if (resultadoPuede.resultado == false) {

              var r = confirm(resultadoPuede.texto);
              if (r == true) {
                var dataEnviar = {
                  cancha: canchaR,
                  dpFecha: dpFechaR,
                  horasReserva: horasReservaR,
                  nombreReserva: nombreReservaR,
                  telReserva: telReservaR,
                  comentarioReserva: comentarioReservaR,
                  fechaDesde: fechaDesdeR,
                  fechaHasta: fechaHastaR,
                  soloDia: soloDiaR,
                  eliminarReservas: 1,
                  idReservaHidden: hiddenModificar
                };
                $.ajax({
                  url: urlR,
                  type: 'POST',
                  data: dataEnviar
                }).done(function (data) {
                  $("#nombreReserva").val("");
                  $("#telReserva").val("");
                  $("#comentarioReserva").val("");
                  eliminarContenidoTr($("#hiddenModificarHora").val());
                  if (data.entreLyD == true && data.id.idDia == $("#idDiaSelected").val() && canchaR == $("#idCanchaSelected").val()) {
                    agregarActualizarTr(horasReservaR, nombreReservaR, telReservaR, comentarioReservaR, data.id.id, selectedJugo);

                    cambioDeColorSuccess(horasReservaR);
                    if (soloDiaR == 1) {
                      setTimeout(function () {
                        $('input[name=idTr' + horasReservaR + ']').parent().parent().addClass("soloDia");
                      }, 1600);
                    }
                  } else {
                    $(".contentCenter").append('<div id="successReserva" class="successBox callout columns large-12 medium-12 small-12 success">Reserva agregada correctamente para la fecha <strong>' + dpFechaR + '</strong> en cancha <strong>' + $("#idCanchaSelect option:selected").text() + '</strong> </div>');
                    setTimeout(function () {
                      $("#successReserva").fadeOut(400, function () {
                        $(this).remove();
                      });
                    }, 5000);

                  }
                  actualizarHorasSelect(($("button.soloDia").hasClass("primary")) ? 1 : 0);
                  $('#modalReserva').foundation('close');
                }).fail(function (data) {
                  var mensaje = (data.status == 500) ? "La fecha Desde debe ser menor o igual a la fecha Hasta." : 'Asegurese de haber ingresado todos los campos requeridos.';

                  $("#modalReserva").append('<div class="columns row"><div class="ErrorBox columns large-12 medium-12 small-12 callout alert">' + mensaje + '</div></div>');
                  $('.reveal-overlay').animate({
                      scrollTop: $('div.ErrorBox').offset().top - 80
                    },
                    700);


                  setTimeout(function () {
                    $("div.ErrorBox").fadeOut(400, function () {
                      $(this).remove();
                    });
                  }, 5000);
                });
              }
            } else {
              var dataEnviar = {
                cancha: canchaR,
                dpFecha: dpFechaR,
                horasReserva: horasReservaR,
                nombreReserva: nombreReservaR,
                telReserva: telReservaR,
                comentarioReserva: comentarioReservaR,
                fechaDesde: fechaDesdeR,
                fechaHasta: fechaHastaR,
                soloDia: soloDiaR,
                idReservaHidden: hiddenModificar
              };
              $.ajax({
                url: urlR,
                type: 'POST',
                data: dataEnviar
              }).done(function (data) {
                $("#nombreReserva").val("");
                $("#telReserva").val("");
                $("#comentarioReserva").val("");


                eliminarContenidoTr($("#hiddenModificarHora").val());
                if (data.entreLyD == true && data.id.idDia == $("#idDiaSelected").val() && canchaR == $("#idCanchaSelected").val()) {
                  agregarActualizarTr(horasReservaR, nombreReservaR, telReservaR, comentarioReservaR, data.id.id, selectedJugo);

                  cambioDeColorSuccess(horasReservaR);
                  if (soloDiaR == 1) {
                    setTimeout(function () {
                      $('input[name=idTr' + horasReservaR + ']').parent().parent().addClass("soloDia");
                    }, 1600);
                  }
                } else {
                  $(".contentCenter").append('<div id="successReserva" class="successBox callout columns large-12 medium-12 small-12 success">Reserva agregada correctamente para la fecha <strong>' + dpFechaR + '</strong> en cancha <strong>' + $("#idCanchaSelect option:selected").text() + '</strong></div>');
                  setTimeout(function () {
                    $("#successReserva").fadeOut(400, function () {
                      $(this).remove();
                    });
                  }, 5000);

                }
                actualizarHorasSelect(($("button.soloDia").hasClass("primary")) ? 1 : 0);
                $('#modalReserva').foundation('close');
              }).fail(function (data) {
                var mensaje = (data.status == 500) ? "La fecha Desde debe ser menor o igual a la fecha Hasta." : 'Asegurese de haber ingresado todos los campos requeridos.';

                $("#modalReserva").append('<div class="columns row"><div class="ErrorBox columns large-12 medium-12 small-12 callout alert">' + mensaje + '</div></div>');
                $('.reveal-overlay').animate({
                    scrollTop: $('div.ErrorBox').offset().top - 80
                  },
                  700);


                setTimeout(function () {
                  $("div.ErrorBox").fadeOut(400, function () {
                    $(this).remove();
                  });
                }, 5000);
              });
            }
          });


        } else {
          var dataEnviar = {
            cancha: canchaR,
            dpFecha: dpFechaR,
            horasReserva: horasReservaR,
            nombreReserva: nombreReservaR,
            telReserva: telReservaR,
            comentarioReserva: comentarioReservaR,
            fechaDesde: fechaDesdeR,
            fechaHasta: fechaHastaR,
            soloDia: soloDiaR,
            idReservaHidden: hiddenModificar
          };
          $.ajax({
            url: urlR,
            type: 'POST',
            data: dataEnviar
          }).done(function (data) {
            $("#nombreReserva").val("");
            $("#telReserva").val("");
            $("#comentarioReserva").val("");


            eliminarContenidoTr($("#hiddenModificarHora").val());
            if (data.entreLyD == true && data.id.idDia == $("#idDiaSelected").val() && canchaR == $("#idCanchaSelected").val()) {

              agregarActualizarTr(horasReservaR, nombreReservaR, telReservaR, comentarioReservaR, data.id.id, selectedJugo);

              cambioDeColorSuccess(horasReservaR);
              if (soloDiaR == 1) {
                setTimeout(function () {
                  $('input[name=idTr' + horasReservaR + ']').parent().parent().addClass("soloDia");
                }, 1600);
              }
            } else {
              $(".contentCenter").append('<div id="successReserva" class="successBox callout columns large-12 medium-12 small-12 success">Reserva agregada correctamente para la fecha <strong>' + dpFechaR + '</strong> en cancha <strong>' + $("#idCanchaSelect option:selected").text() + '</strong></div>');
              setTimeout(function () {
                $("#successReserva").fadeOut(400, function () {
                  $(this).remove();
                });
              }, 5000);

            }
            actualizarHorasSelect(($("button.soloDia").hasClass("primary")) ? 1 : 0);
            $('#modalReserva').foundation('close');
          }).fail(function (data) {
            var mensaje = (data.status == 500) ? "La fecha Desde debe ser menor o igual a la fecha Hasta." : 'Asegurese de haber ingresado todos los campos requeridos.';

            $("#modalReserva").append('<div class="columns row"><div class="ErrorBox columns large-12 medium-12 small-12 callout alert">' + mensaje + '</div></div>');
            $('.reveal-overlay').animate({
                scrollTop: $('div.ErrorBox').offset().top - 80
              },
              700);


            setTimeout(function () {
              $("div.ErrorBox").fadeOut(400, function () {
                $(this).remove();
              });
            }, 5000);
          });
        }
      }

    });


  });



  /*$("#agregarListaNegra").click(function () {
    var nombreL = $("#nombre").val();
    var telL = $("#tel").val();
    var comentarioL = $("#comentario").val();


    var dataEnviar = {
      nombre: nombreL,
      tel: telL,
      comentario: comentarioL
    };
    $.ajax({
      url: "/admin/agregarListaNegra",
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      $("#nombre").val("");
      $("#tel").val("");
      $("#comentario").val("");
      if ($("div.pinned table").length == 1) {
        $("table.responsive").prepend('<tr><td>' + nombreL + '</td><td>' + ((telL!="")?telL:" - ") + '</td><td>' + ((comentarioL!="")?comentarioL:" - ") + '</td><td style="min-width: 176px;"><button type="button" style="float: left;" class="primary button no-margin" onclick="modificarListaNegra(this,' + data.id.id + ');">Editar</button><button type="button" style="float: right;" class="alert button no-margin" onclick="eliminarListaNegra(this,' + data.id.id + ');">Eliminar</button><input type="hidden" name="idTr' + data.id.id + '" /></td></tr>');
        $("div.pinned table").prepend('<tr style="height: 59px;"><td>' + nombreL + '</td><td style="display: none;">' + ((telL!="")?telL:" - ") + '</td><td style="display: none;">' + ((comentarioL!="")?comentarioL:" - ") + '</td><td style="min-width: 176px; display: none;"><button type="button" style="float: left;" class="primary button no-margin" onclick="modificarListaNegra(this,' + data.id.id + ');">Editar</button><button type="button" style="float: right;" class="alert button no-margin" onclick="eliminarListaNegra(this,' + data.id.id + ');">Eliminar</button><input type="hidden" name="idTr' + data.id.id + '"/></td></tr>');
      } else {
        $("table.responsive").prepend('<tr><td>' + nombreL + '</td><td>' + ((telL!="")?telL:" - ") + '</td><td>' + ((comentarioL!="")?comentarioL:" - ") + '</td><td style="min-width: 176px;"><button type="button" style="float: left;" class="primary button no-margin" onclick="modificarListaNegra(this,' + data.id.id + ');">Editar</button><button type="button" style="float: right;" class="alert button no-margin" onclick="eliminarListaNegra(this,' + data.id.id + ');">Eliminar</button><input type="hidden" name="idTr' + data.id.id + '"/></td></tr>');
      }
      
      cambioDeColorSuccess(data.id.id);

    }).fail(function (data) {
      $("#agregarListaNegra").parent().append('<div class="ErrorBox columns large-4 medium-5 small-12 callout alert">El campo nombre no puede estar vacío.</div>');
      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 5000);
    });

  });*/



});

function marcarComoJugado(ele, idR) {
  var jugoR = ($(ele).is(":checked")) ? 1 : 0;

  var dataEnviar = {
    id: idR,
    jugo: jugoR
  };
  $.ajax({
    url: '/admin/cambiarJugoReserva',
    type: 'POST',
    data: dataEnviar
  }).done(function (data) {

  }).fail(function (data) {});

}

function modificarReserva(id, idR) {
  $("#hiddenModificar").val(idR);
  var dataEnviar = {
    id: idR
  };
  $.ajax({
    url: '/admin/getRegistroModificarReserva',
    type: 'POST',
    data: dataEnviar
  }).done(function (data) {

    $("#idCanchaSelect").val(data.data[0].idCancha);
    $("#dpFecha").val(data.data[0].fecha);
    $("#horasReserva").val(data.data[0].idHora);
    $('#horasReserva option[value="' + data.data[0].idHora + '"]').removeAttr('disabled');
    $('#horasReserva option[value="' + data.data[0].idHora + '"]').html($('#horasReserva option[value="' + data.data[0].idHora + '"]').text().substring(0, 5) + " (hora actual)");
    $("#nombreReserva").val(data.data[0].nombre);
    $("#telReserva").val(data.data[0].telefono);
    $("#comentarioReserva").val(data.data[0].comentario);
    $("#dpFechaDesde").val((data.data[0].fechaDesdeNoJuega == '0000-00-00') ? '' : data.data[0].fechaDesdeNoJuega);
    $("#dpFechaHasta").val((data.data[0].fechaHastaNoJuega == '0000-00-00') ? '' : data.data[0].fechaHastaNoJuega);
    $("#hiddenModificar").val(idR);
    $("#hiddenModificarHora").val(id);


    if (data.data[0].soloHoy == 0) {
      $("#contentFechaDesdeHasta").show();
      $("button.semanal").removeClass("secondary").addClass("primary");
      $("button.soloDia").removeClass("primary").addClass("secondary");
    }


    $('#modalReserva h3').html("Modificar reserva");
    $('#guardarReservaSoloDia').html("Actualziar");
    $('#modalReserva').foundation('open');
  }).fail(function (data) {

  });
}

function expandirOcultarContenido(ele) {

  if ($(ele).next().is(':visible')) {
    $(ele).next().hide();
    $("span.colapseExpandIcon").html("+");
  } else {
    $("span.colapseExpandIcon").html("+");
    $(ele).find("span.colapseExpandIcon").html("-");
    $("div.table-expand-row-content").hide();
    $(ele).next().show();
  }
};

function eliminarTipo(idT, tipoT, ele) {
  var r = confirm("Si se elimina el tipo de servicio se eliminaran las canchas asociadas asi como también las reservas de dicha cancha. ¿Desea eliminar el registro de forma permanente?");
  if (r == true) {
    var dataEnviar = {
      id: idT,
      tipo: tipoT
    };
    $.ajax({
      url: '/admin/eliminarTipo',
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      $("#trAbajo" + idT).remove();
      $("#trArriba" + idT).remove();

      /*$(ele).parent().parent().append('<div class="successBox callout columns large-12 medium-12 small-12 success">'+data+'</div>');
      setTimeout(function () {
        $("div.successBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 3000);*/
    }).fail(function (data) {
      $(ele).parent().parent().append('<div class="ErrorBox columns large-12 medium-12 small-12 callout alert">Ocurrio un error inesperado, intentelo de nuevo.</div>');

      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 3000);
    });
  }
}

function eliminarListaNegra(ele, idT) {
  var r = confirm("¿Desea eliminar el registro de forma permanente?");
  if (r == true) {
    var dataEnviar = {
      id: idT
    };
    $.ajax({
      url: '/admin/eliminarListaNegra',
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {

      $('input[name=idTr' + idT + ']').parent().parent().remove();
      /*$(ele).parent().parent().append('<div class="successBox callout columns large-12 medium-12 small-12 success">'+data+'</div>');
      setTimeout(function () {
        $("div.successBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 3000);*/
    }).fail(function (data) {
      /*$(ele).parent().parent().append('<div class="ErrorBox columns large-12 medium-12 small-12 callout alert">Ocurrio un error inesperado, intentelo de nuevo.</div>');
      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 3000);*/
      console.log(data);

    });
  }
}


function eliminarCancha(idT, ele) {
  var r = confirm("Si se elimina la cancha también serán eliminadas todas las reservas vinculadas a la misma, ¿Desea continuar?");
  if (r == true) {
    var dataEnviar = {
      id: idT
    };
    $.ajax({
      url: '/admin/eliminarCancha',
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      $("#trAbajo" + idT).remove();
      $("#trArriba" + idT).remove();
      console.log(data);

      /*$(ele).parent().parent().append('<div class="successBox callout columns large-12 medium-12 small-12 success">'+data+'</div>');
      setTimeout(function () {
        $("div.successBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 3000);*/
    }).fail(function (data) {
      $("#trAbajo" + idT + " div.rowModificarEliminar").append('<div class="ErrorBox columns large-12 medium-12 small-12 callout alert">Ocurrio un error inesperado, intentelo de nuevo.</div>');
      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 3000);
    });
  }
}

function eliminarServicio(idT, ele) {
  var r = confirm("¿Desea eliminar el registro de forma permanente?");
  if (r == true) {
    var dataEnviar = {
      id: idT
    };
    $.ajax({
      url: '/admin/eliminarServicio',
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      $("#trAbajo" + idT).remove();
      $("#trArriba" + idT).remove();

      /*$(ele).parent().parent().append('<div class="successBox callout columns large-12 medium-12 small-12 success">'+data+'</div>');
      setTimeout(function () {
        $("div.successBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 3000);*/
    }).fail(function (data) {
      $("#trAbajo" + idT + " div.rowModificarEliminar").append('<div class="ErrorBox columns large-12 medium-12 small-12 callout alert">Ocurrio un error inesperado, intentelo de nuevo.</div>');
      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 3000);
    });
  }
}



function guardarModificarCancha(idT, ele) {
  var nombreC = $("#nombreCancha" + idT).val();
  var tipoCC = $("#selectTipoC" + idT).val();
  var tipoPC = $("#selectTipoP" + idT).val();
  var permiteCumpleC = $("#permiteCumple" + idT).val();
  var precioCanchaC = $("#precioCancha" + idT).val();

  if (nombreC != '' && precioCanchaC != '') {


    var dataEnviar = {
      nombre: nombreC,
      tipoC: tipoCC,
      tipoP: tipoPC,
      permiteCumple: permiteCumpleC,
      precio: precioCanchaC,
      id: idT
    };
    $.ajax({
      url: '/admin/modificarCancha',
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      $("#nombreId" + idT).html(nombreC);
      $("#trAbajo" + idT + " div.rowModificarEliminar").append('<div class="successBox callout columns large-12 medium-12 small-12 success">' + data + '</div>');
      setTimeout(function () {
        $("div.successBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 3000);
    }).fail(function (data) {
      $("#trAbajo" + idT + " div.rowModificarEliminar").append('<div class="ErrorBox columns large-12 medium-12 small-12 callout alert">Ocurrio un error inesperado, intentelo de nuevo.</div>');
      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 6000);
    });
  } else {
    $("#trAbajo" + idT + " div.rowModificarEliminar").append('<div class="ErrorBox callout columns large-12 medium-12 small-12 alert">El campo nombre y precio no puede estar vacío.</div>');
    setTimeout(function () {
      $("div.ErrorBox").fadeOut(400, function () {
        $(this).remove();
      });
    }, 6000);
  }

}

function modificarListaNegra(ele, idT) {
  var dataEnviar = {
    id: idT
  };
  $.ajax({
    url: '/admin/getRegistroModificarListaNegra',
    type: 'POST',
    data: dataEnviar
  }).done(function (data) {
    $("#nombre").val(data.data[0].nombre);
    $("#tel").val(data.data[0].telefono);
    $("#comentario").val(data.data[0].comentario);
    $("#hiddenModificar").val(idT);
    $('#modalListaN h3').html("Modificar registro");
    $('#guardarListaNegra').html("Actualziar");
    $('#modalListaN').foundation('open');
  }).fail(function (data) {

  });

}

function guardarModificarServicio(idT, ele) {
  var nombreS = $("#nombreServicio" + idT).val();
  var tipoSS = $("#selectTipoS" + idT).val();
  var precioServicioS = $("#precioServicio" + idT).val();

  if (nombreS != '' && precioServicioS != '') {


    var dataEnviar = {
      nombre: nombreS,
      tipoS: tipoSS,
      precio: precioServicioS,
      id: idT
    };
    $.ajax({
      url: '/admin/modificarServicio',
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      $("#nombreId" + idT).html(nombreS);
      $("#trAbajo" + idT + " div.rowModificarEliminar").append('<div class="successBox callout columns large-12 medium-12 small-12 success">' + data + '</div>');
      setTimeout(function () {
        $("div.successBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 3000);
    }).fail(function (data) {
      $("#trAbajo" + idT + " div.rowModificarEliminar").append('<div class="ErrorBox columns large-12 medium-12 small-12 callout alert">Ocurrio un error inesperado, intentelo de nuevo.</div>');
      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 6000);
    });
  } else {
    $("#trAbajo" + idT + " div.rowModificarEliminar").append('<div class="ErrorBox callout columns large-12 medium-12 small-12 alert">El campo nombre y precio no puede estar vacío.</div>');
    setTimeout(function () {
      $("div.ErrorBox").fadeOut(400, function () {
        $(this).remove();
      });
    }, 6000);
  }

}






function guardarModificar(idT, tipoT, ele) {
  var valor = $(ele).parent().parent().find("input").val();
  if (valor != '') {


    var dataEnviar = {
      nombre: valor,
      id: idT,
      tipo: tipoT
    };
    $.ajax({
      url: '/admin/guardarModificarTipo',
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      $("#nombreId" + idT).html(valor);
      $(ele).parent().parent().append('<div class="successBox callout columns large-12 medium-12 small-12 success">' + data + '</div>');
      setTimeout(function () {
        $("div.successBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 3000);
    }).fail(function (data) {
      $(ele).parent().parent().append('<div class="ErrorBox columns large-12 medium-12 small-12 callout alert">Ocurrio un error inesperado, intentelo de nuevo.</div>');
      setTimeout(function () {
        $("div.ErrorBox").fadeOut(400, function () {
          $(this).remove();
        });
      }, 6000);
    });
  } else {
    $(ele).parent().parent().append('<div class="ErrorBox callout columns large-12 medium-12 small-12 alert">El campo nombre no puede estar vacío.</div>');
    setTimeout(function () {
      $("div.ErrorBox").fadeOut(400, function () {
        $(this).remove();
      });
    }, 6000);
  }

}

function desplegarAgregarListaNegra() {

  $("#nombre").val("");
  $("#tel").val("");
  $("#comentario").val("");
  $("#hiddenModificar").val("-1");
  $('#modalListaN h3').html("Agregar registro");
  $('#guardarListaNegra').html("Agregar");
  $('#modalListaN').foundation('open');
  /*if ($(".contenidoAgregarListaNegra").is(":visible")) {
    $(".contenidoAgregarListaNegra").hide();
  } else {
    $(".contenidoAgregarListaNegra").css('display', 'flex');
  }*/


}

function funcionBuscar() {
  // Declare variables
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table1 = $("table.responsive");
  table2 = $("div.pinned table");



  if ($("div.pinned table").length == 1) {
    tr = $("table.responsive tr");
    tr1 = $("div.pinned table tr");
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[0];
      td2 = tr[i].getElementsByTagName("td")[2];
      if (td) {
        if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
          tr1[i].style.display = "";
        } else {
          tr[i].style.display = "none";
          tr1[i].style.display = "none";
        }
      }
    }
  } else {
    tr = $("table.responsive tr");
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[0];
      td2 = tr[i].getElementsByTagName("td")[2];
      if (td) {
        if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }
    }
  }

}

/*Obtengo el color del fondo*/
function hexc(colorval) {
  var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
  delete(parts[0]);
  for (var i = 1; i <= 3; ++i) {
    parts[i] = parseInt(parts[i]).toString(16);
    if (parts[i].length == 1) parts[i] = '0' + parts[i];
  }
  return ('#' + parts.join(''));
}

function cambioDeColorSuccess(id) {


  var x = $('input[name=idTr' + id + ']').parent().parent().css('backgroundColor');

  if (x == 'rgba(0, 0, 0, 0)') {
    var color = "#FFFFFF"
  } else {
    var color = hexc(x);
  }
  console.log(color);


  $('input[name=idTr' + id + ']').parent().parent().stop().animate({
    backgroundColor: '#22bb5b'
  }, 800, function () {
    $('input[name=idTr' + id + ']').parent().parent().stop().animate({
      backgroundColor: color
    }, 800);
  });

}

function cambioDeColorError(id) {

  var x = $('input[name=idTr' + id + ']').parent().parent().css('backgroundColor');

  if (x == 'rgba(0, 0, 0, 0)') {
    var color = "#FFFFFF"
  } else {
    var color = hexc(x);
  }
  
  $('input[name=idTr' + id + ']').parent().parent().stop().animate({
    backgroundColor: '#cb2328'
  }, 800, function () {
    $('input[name=idTr' + id + ']').parent().parent().stop().animate({
      backgroundColor: color
    }, 800);
  });
}

function agregarReserva(ele, idHora, idC) {
  $("#horasReserva").val(idHora);
  $("#idCanchaSelect").val(idC);
  $('#modalReserva h3').html("Reserva");
  $('#guardarReservaSoloDia').html("Ingresar");
  $('#modalReserva').foundation('open');
}

function cargarReservasCanchaDiaHistorial() {
  var dataEnviar = {
    id: $("#idCanchaSelected").val(),
    fecha: $("#dpFechaHistorial").val()
  };
  $.ajax({
    url: '/admin/cargarHistoralCancha',
    type: 'POST',
    data: dataEnviar
  }).done(function (data) {
    $("#canchaDia").html(data.canchaDia);
    if ($("div.pinned table").length == 1) {
      $("table.responsive").fadeOut('fast', function () {
        $(this).html('<thead><tr><th>Hora</th><th>Nombre</th><th>Teléfono</th><th>Comentario</th><th>¿Jugó?</th><th>Acciones</th></tr></thead><tbody>' + data.resultado + '</tbody>').fadeIn('fast');
      });
      $("div.pinned table").fadeOut('fast', function () {
        $(this).html('<thead><tr><th>Hora</th><th>Nombre</th><th>Teléfono</th><th>Comentario</th><th>¿Jugó?</th><th>Acciones</th></tr></thead><tbody>' + data.resultado + '</tbody>').fadeIn('fast');
      });
    } else {
      $("table.responsive").fadeOut('fast', function () {
        $(this).html('<thead><tr><th>Hora</th><th>Nombre</th><th>Teléfono</th><th>Comentario</th><th>¿Jugó?</th><th>Acciones</th></tr></thead><tbody>' + data.resultado + '</tbody>').fadeIn('fast');
      });
    }

  }).fail(function (data) {
    console.log(data);
  });
}

function cargarReservasCanchaDia() {
  var dataEnviar = {
    id: $("#idCanchaSelected").val(),
    dia: $("#idDiaSelected").val()
  };
  $.ajax({
    url: '/admin/cargarReservasCancha',
    type: 'POST',
    data: dataEnviar
  }).done(function (data) {
    $("#canchaDia").html(data.canchaDia);

    $("#dpFecha").val(data.fechaEsMenor);

    $("#fechaSemanaSelected").val(data.fechaEsMenor);
    $("#horasReserva").html(data.horasDisponibles);

    if ($("div.pinned table").length == 1) {
      $("table.responsive").fadeOut('fast', function () {
        $(this).html('<thead><tr><th>Hora</th><th>Nombre</th><th>Teléfono</th><th>Comentario</th><th>¿Jugó?</th><th>Acciones</th></tr></thead><tbody>' + data.resultado + '</tbody>').fadeIn('fast');
      });
      $("div.pinned table").fadeOut('fast', function () {
        $(this).html('<thead><tr><th>Hora</th><th>Nombre</th><th>Teléfono</th><th>Comentario</th><th>¿Jugó?</th><th>Acciones</th></tr></thead><tbody>' + data.resultado + '</tbody>').fadeIn('fast');
      });
    } else {
      $("table.responsive").fadeOut('fast', function () {
        $(this).html('<thead><tr><th>Hora</th><th>Nombre</th><th>Teléfono</th><th>Comentario</th><th>¿Jugó?</th><th>Acciones</th></tr></thead><tbody>' + data.resultado + '</tbody>').fadeIn('fast');
      });
    }

  }).fail(function (data) {
    console.log(data);
  });
}

function cambiarCancha(ele, id, tipo) {
  $(".itemCancha span.selected").removeClass("selected");
  $(ele).addClass("selected");
  $("#idCanchaSelected").val(id);
  if (tipo == 1) {
    cargarReservasCanchaDia();
    $('html, body').animate({
        scrollTop: $('#containerDias').offset().top - 80
      },
      400);
  } else {
    cargarReservasCanchaDiaHistorial();
  }

}

function cambiarDia(ele, id) {
  $(".itemDia span.selected").removeClass("selected");
  $(ele).addClass("selected");
  $("#idDiaSelected").val(id);
  cargarReservasCanchaDia();
  $('html, body').animate({
      scrollTop: $('#canchaDia').offset().top - 20
    },
    400);
}

/**Aca actualizo las horas del select */

function actualizarHorasSelect(soloDiaR) {
  var valor = $("#dpFecha").val();
  var canchaF = $("#idCanchaSelect").val();
  var dataEnviar = {
    fecha: valor,
    cancha: canchaF,
    soloDia: soloDiaR
  };
  $.ajax({
    url: "/admin/darHorasFecha",
    type: 'POST',
    data: dataEnviar
  }).done(function (data) {
    $("#horasReserva").html(data.horasDisponibles);
  }).fail(function (data) {

  });
}

function corroborarFechaSoloDiaParaSemanal(handleData) {
  var valor = $("#dpFecha").val();
  var canchaF = $("#idCanchaSelect").val();
  var horaF = $("#horasReserva").val();
  var fechaDesdeR = ($("#dpFechaDesde").val() != '' && $("#dpFechaHasta").val() != '') ? $("#dpFechaDesde").val() : null;
  var fechaHastaR = ($("#dpFechaDesde").val() != '' && $("#dpFechaHasta").val() != '') ? $("#dpFechaHasta").val() : null;
  var dataEnviar = {
    fecha: valor,
    cancha: canchaF,
    hora: horaF,
    fechaDesde: fechaDesdeR,
    fechaHasta: fechaHastaR
  };
  //var resultado = true;
  $.ajax({
    url: "/admin/corroborarFechaSoloDiaParaSemanal",
    type: 'POST',
    data: dataEnviar
  }).done(function (data) {
    handleData(data);
  }).fail(function (data) {
    console.log(data);
  });
}

function chequearPersonaEnListaNegra(handleData) {
  var telR = $("#telReserva").val();
  var dataEnviar = {
    tel: telR
  };
  $.ajax({
    url: "/admin/chequearPersonaEnListaNegra",
    type: 'POST',
    data: dataEnviar
  }).done(function (data) {
    handleData(data);
  }).fail(function (data) {
    console.log(data);
  });
}

function cargarSoloPorDiaSemanal(tipo) {
  if (tipo == 1) {
    $("#contentFechaDesdeHasta").fadeOut(200);
    $("button.soloDia").removeClass("secondary").addClass("primary");
    $("button.semanal").removeClass("primary").addClass("secondary");
    actualizarHorasSelect(($("button.soloDia").hasClass("primary")) ? 1 : 0);
  } else {
    $("#contentFechaDesdeHasta").fadeIn(200);
    $("button.semanal").removeClass("secondary").addClass("primary");
    $("button.soloDia").removeClass("primary").addClass("secondary");
    actualizarHorasSelect(($("button.soloDia").hasClass("primary")) ? 1 : 0);
  }
}



function eliminarHistorial(id, idR) {
  var r = confirm("¿Desea eliminar la reserva del historial de forma permanente?");
  if (r == true) {
    var dataEnviar = {
      id: idR
    };
    $.ajax({
      url: '/admin/eliminarHistorial',
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      setTimeout(function () {
        $('input[name=idTr' + id + ']').parent().parent().find("td:eq(1)").text('');
        $('input[name=idTr' + id + ']').parent().parent().find("td:eq(2)").text('');
        $('input[name=idTr' + id + ']').parent().parent().find("td:eq(3)").text('');
        $('input[name=idTr' + id + ']').parent().parent().find("td:eq(4)").text('');
        $('input[name=idTr' + id + ']').parent().parent().find("td:eq(5)").html('<input type="hidden" name="idTr' + id + '" />');
      }, 800);
      $('input[name=idTr' + id + ']').parent().parent().removeClass("soloDia");
      $('input[name=idTr' + id + ']').parent().parent().removeClass("soloDiasoloDiaCumple");
      cambioDeColorError(id);          
    }).fail(function (data) {      
      console.log(data);

    });
  }


}

function eliminarReserva(id, idR) {
  var r = confirm("¿Desea eliminar la reserva de forma permanente?");
  if (r == true) {
    var dataEnviar = {
      id: idR
    };
    $.ajax({
      url: '/admin/eliminarReserva',
      type: 'POST',
      data: dataEnviar
    }).done(function (data) {
      setTimeout(function () {
        //$('input[name=idTr' + id + ']').parent().parent().find("td:eq(1)").text('');
        $('input[name=idTr' + id + ']').parent().parent().find("td:eq(1)").text('');
        $('input[name=idTr' + id + ']').parent().parent().find("td:eq(2)").text('');
        $('input[name=idTr' + id + ']').parent().parent().find("td:eq(3)").text('');
        $('input[name=idTr' + id + ']').parent().parent().find("td:eq(4)").text('');
        $('input[name=idTr' + id + ']').parent().parent().find("td:eq(5)").html('<i class="fa fa-plus-circle" onclick="agregarReserva(this,' + id + ',' + $("#idCanchaSelected").val() + ');"></i><input type="hidden" name="idTr' + id + '" />');
      }, 800);
      $('input[name=idTr' + id + ']').parent().parent().removeClass("soloDia");
      $('input[name=idTr' + id + ']').parent().parent().removeClass("soloDiasoloDiaCumple");
      cambioDeColorError(id);
      actualizarHorasSelect(($("button.soloDia").hasClass("primary")) ? 1 : 0);     
    }).fail(function (data) {      
      console.log(data);

    });
  }


}

function eliminarContenidoTr(id) {
  console.log(id);

  //$('input[name=idTr' + id + ']').parent().parent().find("td:eq(1)").text('');
  $('input[name=idTr' + id + ']').parent().parent().find("td:eq(1)").text('');
  $('input[name=idTr' + id + ']').parent().parent().find("td:eq(2)").text('');
  $('input[name=idTr' + id + ']').parent().parent().find("td:eq(3)").text('');
  $('input[name=idTr' + id + ']').parent().parent().find("td:eq(4)").text('');
  $('input[name=idTr' + id + ']').parent().parent().find("td:eq(5)").html('<i class="fa fa-plus-circle" onclick="agregarReserva(this,' + id + ',' + $("#idCanchaSelected").val() + ');"></i><input type="hidden" name="idTr' + id + '" />');
  $('input[name=idTr' + id + ']').parent().parent().removeClass("soloDia");
  $('input[name=idTr' + id + ']').parent().parent().removeClass("soloDiasoloDiaCumple");
}

function agregarActualizarTr(horasReservaR, nombreReservaR, telReservaR, comentarioReservaR, idR, selectedJugo) {
  //$('input[name=idTr' + horasReservaR + ']').parent().parent().find("td:eq(1)").text('1');
  $('input[name=idTr' + horasReservaR + ']').parent().parent().find("td:eq(1)").text(nombreReservaR);
  $('input[name=idTr' + horasReservaR + ']').parent().parent().find("td:eq(2)").text(telReservaR);
  $('input[name=idTr' + horasReservaR + ']').parent().parent().find("td:eq(3)").text(comentarioReservaR);
  $('input[name=idTr' + horasReservaR + ']').parent().parent().find("td:eq(4)").html('<input id="jugo' + idR + '" type="checkbox" ' + selectedJugo + ' onclick="marcarComoJugado(this, ' + idR + ');"><label for="jugo' + idR + '" class="labelJugo">¿Jugo?</label>');
  $('input[name=idTr' + horasReservaR + ']').parent().parent().find("td:eq(5)").html('<i class="fa fa-edit" onclick="modificarReserva(' + horasReservaR + ',' + idR + ');"></i><i class="fas fa-trash-alt" onclick="eliminarReserva(' + horasReservaR + ',' + idR + ');"></i><input type="hidden" name="idReserva' + idR + '" /><input type="hidden" name="idTr' + horasReservaR + '" />');
  $('input[name=idTr' + horasReservaR + ']').parent().parent().removeClass("soloDia");
  $('input[name=idTr' + horasReservaR + ']').parent().parent().removeClass("soloDiasoloDiaCumple");
}