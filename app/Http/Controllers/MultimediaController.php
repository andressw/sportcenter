<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class MultimediaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function cargarVistaMultimedia()
    {
        return view('multimedia', ['seccion' => 'multimedia', "tituloSección" => 'Multimedia']);
    }

    public function cargarVistaListarCancha()
    {
        /*$art = DB::table('articulos')
        ->join('categorias', 'categorias.id', '=', 'articulos.idCategoria')
        ->select('articulos.*', 'categorias.titulo as tituloC')
        ->where('articulos.estado', 1)
        ->orderby('articulos.updated_at', 'desc')
        ->get();

        $artBorradorr = DB::table('articulos')
        ->where('articulos.estado', 0)->count();
        $conf = DB::table('configuracion')->get();*/
        $tipoP = DB::table('tipo_pastos')->get();
        $tipoC = DB::table('tipo_canchas')->get();
        $cancha = DB::table('canchas')->get();
        return view('listar_cancha', ['seccion' => 'listarCancha', "tituloSección" => 'Listar canchas', 'tipoC' =>$tipoC, 'canchas' =>$cancha, 'tipoP' =>$tipoP]);
    }

    
    
    public function modificarCancha(Request $request)
    {
        $this->validate($request, ["nombre" => 'required|min:1',
        "permiteCumple" => 'required|min:1',
        "precio" => 'required|min:1',
        "tipoP" => 'required|min:1',
        "tipoC" => 'required|min:1',
        ]);

        DB::table('canchas')
            ->where('id', $request->id)
            ->update([
                'nombre' => $request->nombre,
                'precio' => $request->precio,
                'permiteCumpleanos' => $request->permiteCumple,
                'tipoCancha' => $request->tipoC,
                'tipoPasto' => $request->tipoP
                ]);
        return response('Cancha modificada correctamente.', 200)
            ->header('Content-Type', 'text/plain');
    }
    public function eliminarCancha(Request $request)
    {
        
        /*
        Hacer el buvcle apra que elimine todas las reservas
        */

        DB::table('canchas')
            ->where('id', $request->id)
            ->delete();
        return response('Cancha eliminada correctamente.', 200)
            ->header('Content-Type', 'text/plain');
    }

    public function ingresarCancha(Request $request)
    {
        $this->validate($request, ["nombre" => 'required|min:1',
        "precio" => 'required|min:1',
        "permiteCumple" => 'required|min:1',
        "selectTipoP" => 'required|min:1',
        "selectTipoC" => 'required|min:1',
        ]);

        $cancha = new Cancha();
        $cancha->nombre = $request->nombre;
        $cancha->tipoCancha = $request->selectTipoC;
        $cancha->tipoPasto = $request->selectTipoP;
        $cancha->permiteCumpleanos = $request->permiteCumple;
        $cancha->precio = $request->precio;
        

        if (!$cancha->save()) {
            return response('Error al ingresar el registro, intentelo nuevamente', 500)
                ->header('Content-Type', 'text/plain');
        } else {
            return response('Cancha ingresada correctamente.', 200)
                ->header('Content-Type', 'text/plain');
        }

    }    

    public function resizeImagePost($image)
  {

    //para llamar a esta funcion
    /*if($request->file('fileImagen') != null){
            $this->resizeImagePost($request->file('fileImagen'));
        }*/
   
    //$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
    $input['imagename'] = $image->getClientOriginalName();


    $destinationPath = public_path('static/thumbnail');
    $img = Image::make($image->getRealPath());
    $ancho = (35000/$img->width());
    $img->resize(350, (($img->height()*$ancho)/100), function ($constraint) {
      $constraint->aspectRatio();
    })->save($destinationPath.'/'.$input['imagename']);

    $destinationPath = public_path('static/images-panel');
    $image->move($destinationPath, $input['imagename']);
    /*Copia la imagen a original*/
    $destinationPathOrigin = public_path('static/images-original');
    \File::copy($destinationPath.'/'.$input['imagename'] , $destinationPathOrigin.'/'.$input['imagename'] );
    /*---------------*/
    return $this->postImage($image->getClientOriginalName());

  }

}
