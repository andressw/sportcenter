@include('layouts.head_panel')
<div class="contentCenter columns large-10 large-offset-2 medium-12 small-12">
  <div class="contentFormulario columns large-7 medium-12 small-12">


@if (count($tipos) > 0)
    <div class="tituloTablaTr divComoTr table-expand-row columns large-12 medium-12 small-12">Nombre</div>
    @for ($i = 0; $i
    < count($tipos); $i++) <div id="trArriba{{$tipos[$i]->id}}" class="divComoTr table-expand-row columns large-12 medium-12 small-12" onclick="expandirOcultarContenido(this);">
      <span id="nombreId{{$tipos[$i]->id}}">{{$tipos[$i]->nombre}}</span>
      <span class="colapseExpandIcon">+</span>
  </div>
  <div id="trAbajo{{$tipos[$i]->id}}" class="divAbajoClass divComoTr table-expand-row-content columns large-12 medium-12 small-12">
    <div class="row rowModificarEliminar">
      <input type="text" placeholder="Nombre" value="{{$tipos[$i]->nombre}}">
      <div class="divContentSuccess columns large-6 medium-6 small-6">
        <button class="enviarAgregarListar buttonCustomSuccess button success" type="button" onclick="guardarModificar({{$tipos[$i]->id}},1,this);">Guardar</button>
      </div>
      <div class="divContentAlert columns large-6 medium-6 small-6">
        <button class="enviarAgregarListar button alert" type="button" onclick="eliminarTipo({{$tipos[$i]->id}},1,this);">Eliminar</button>
      </div>
    </div>
  </div>
  @endfor
  @else
    <div class="noRegistrosIngresados columns large-12 medium-12 small-12">
      <span>No se encontraron registros ingresados.</span>
    </div>
    @endif

  </div>
</div>
@include('layouts.footer_panel')