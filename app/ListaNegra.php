<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaNegra extends Model
{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'nombre', 'telefono', 'comentario'
  ];

}
