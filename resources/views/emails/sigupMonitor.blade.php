<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Contact from HTTP Response Monitor</title>
    <style media="screen">
      div{
        float: left;
        width: 100%;
      }
      h4{
        padding-bottom: 10px;
      }
      label, span, p{
        float: left;
        width: 100%;
      }
      label{
        font-weight: bold;
        margin-bottom: 3px;
        color: #444;
      }
      a{
        font-weight: bold;
      }
      span, p{
        margin-bottom: 10px;
        color: #777;
      }
    </style>
  </head>
  <body>
    @if( ! empty($datos))
    <div>
      <h4>Contact from HTTP Response Monitor</h4>
      <p>Welcome to HTTP Response Monitor, you can enter to your control panel on this <a href="{{$datos['urlLogin']}}" target="_blank">url</a> using your email account and the password below. </br>
      <strong>{{$datos['pass']}}</strong>
      </p>
    </div>
    @else
    <div>
      error
    </div>
    @endif
  </body>
</html>
