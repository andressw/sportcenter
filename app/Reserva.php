<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'idHora','idDia', 'idCancha', 'nombre', 'telefono', 'comentario', 'fecha', 'jugado', 'fechaDesdeNoJuega', '	fechaHastaNoJuega','soloHoy','hoyJuega'
  ];
}
