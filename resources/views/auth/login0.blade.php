<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login Infranetworking</title>

  <link href="/css/base.css" rel="stylesheet"/>
  <link href="/css/customPanel.css" rel="stylesheet"/>
</head>
<body>
                <div class="large-12 columns login">
                <div class="large-3 large-centered columns container">
                  <h2>Login Infranetworking</h2>
                    <form class="logiForm" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>


                            </div>
                        </div>

                        <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>

</body>
</html>
