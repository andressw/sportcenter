<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SportCenter.com.uy - Canchas de F&uacute;tbol</title>
<link href="static/img/favicon.ico" rel='shortcut icon' type='image/x-icon'/>
<link href="static/css/index.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Squada+One' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="static/css/vlightbox.css" type="text/css" media="screen"/>
<style type="text/css">#vlightbox a#vlb{display:none}</style>
<link rel="stylesheet" href="static/css/visuallightbox.css" type="text/css" media="screen" />
<!--<script src="static/js/jquery" type="text/javascript"></script>-->
<script type="text/javascript" src="static/js/jquery.js"></script>
<script src="static/js/jquery.validate.js" type="text/javascript"></script>
</head>
<body>

	<div class="header center">
    	<div class="logo"><a href="#"><img src="static/img/logo.png" /></a></div>
        <div class="tel"></div>
    </div>
    <div class="top"></div>

	<div class="cont_1 center">
        <div class="listado">
            <ul>
                <li style="color:#FFFFFF">Canchas de F&uacute;tbol 5 techadas</li>
                <li>Cancha de F&uacute;tbol R&aacute;pido &uacute;nica en el pa&iacute;s</li>
                <li style="color:#FFFFFF">Areas exclusivas para Socios del Club</li>
                <li>Partidos televisados en nuestras pantallas</li>
                <li style="color:#FFFFFF">Celebraci&oacute;n de reuniones y cumplea&ntilde;os</li>
                <li>Tarifas especiales para campeonatos</li>
            </ul>
        </div>
        <div class="dir"># Francisco Aguilar casi Sarand&iacute;. Maldonado - Uruguay </div>
        <div class="mapa">
        	<div class="foto1">
            	<a href="static/img/cancha1.jpg" class="vlightbox" title=""><img src="static/img/cancha1_t.jpg" /></a>
        	</div>
            <div class="foto2">
            	<a href="static/img/cancha2.jpg" class="vlightbox" title=""><img src="static/img/cancha2_t.jpg" /></a>
        	</div>
        </div>
    </div>

    <div class="sportcenter">
    	<div class="nos center">
        	<b style="color:#FFFF00">SportCenter.com.uy, el lugar pensado para practicar tu deporte favorito: el F&uacute;tbol</b><br /><br />
 			Hicimos SportCenter.com.uy con el &uacute;nico prop&oacute;sito de disfrutar de la pr&aacute;ctica del f&uacute;tbol.
            Los dise&ntilde;adores del complejo, hemos sido usuarios por a&ntilde;os de varias canchas de f&uacute;tbol 5.
            Nuestra meta es brindar el mejor servicio, cuidando hasta el m&aacute;s m&iacute;nimo detalle, favoreciendo as&iacute; la comodidad del cliente.
            El complejo deportivo SportCenter.com.uy, es un sue&ntilde;o y un orgullo para nosotros. <br />
            Creamos este proyecto (y lo cuidamos) con much&iacute;simo cari&ntilde;o.
   		</div>
    </div>

    <div class="cancha center">
    	<div class="futbol_5" align="left">
        	<div class="futbol_texto">
                <b style="font-size:14px;color:#FFFF00; ">F&Uacute;TBOL 5:</b><br /><br />
                SportCenter.com.uy cuenta con dos canchas de f&uacute;tbol 5:<br />
                Una cancha grande y otra chica. Ambas canchas son totalmente techadas.
                En la cancha grande, existe la posibilidad de observar el partido desde el
                circuito de televisi&oacute;n instalado en el complejo.
                Por supuesto, los partidos se pueden observar desde las tribunas tambi&eacute;n.
            </div><br />
            <a href="static/img/cancha3.jpg" class="vlightbox" title=""><img src="static/img/cancha3_t.jpg" /></a>
            <a href="static/img/cancha4.jpg" class="vlightbox" title=""><img src="static/img/cancha4_t.jpg" /></a>
            <a href="static/img/cancha5.jpg" class="vlightbox" title=""><img src="static/img/cancha5_t.jpg" /></a><br /><br />

            <a href="static/pdf/Reglas_futbol5.pdf" target="_blank">REGLAMENTO OFICIAL F&Uacute;TBOL 5</a><br />
        </div>
        <div class="futbol_rapido" align="right">
        <div class="futbol_texto">
        	<b style="font-size:14px;color:#FFFF00">F&Uacute;TBOL R&Aacute;PIDO:</b><br /><br />
 			SportCenter.com.uy presenta una cancha de f&uacute;tbol r&aacute;pido &uacute;nica en el pa&iacute;s.
            Totalmente dise&ntilde;ada con una tecnolog&iacute;a moderna, que permite una
            excelente visualizaci&oacute;n y juego. Esta cancha, cuenta adem&aacute;s con la
            posibilidad de observar el juego desde el circuito de televisi&oacute;n
            instalado en el complejo.
        </div><br />

            <a href="static/img/cancha6.jpg" class="vlightbox" title=""><img src="static/img/cancha6_t.jpg" /></a>
            <a href="static/img/cancha7.jpg" class="vlightbox" title=""><img src="static/img/cancha7_t.jpg" /></a>
            <a href="static/img/cancha8.jpg" class="vlightbox" title=""><img src="static/img/cancha8_t.jpg" /></a><br /><br />

            <a href="static/pdf/Reglas_futbolr.pdf" target="_blank">REGLAMENTO OFICIAL F&Uacute;TBOL R&Aacute;PIDO</a><br />
        </div>
    </div>

    <div class="pie_">
    	<div class="pie">
        	<img src="static/img/pie.png" />
            Francisco Aguilar casi Sarand&iacute;. Maldonado - Uruguay <br />
            4224 4513 | 095 931 646 <br />
            <a href="mailto:reservas@sportcenter.com.uy">reservas@sportcenter.com.uy</a> | <a href="#"> www.sportcenter.com.uy </a> <br />
        </div>
    </div>
<script>
  var $VisualLightBoxParams$ = {autoPlay:true,borderSize:10,enableSlideshow:true,startZoom:true};
</script>
<script src="static/js/visuallightbox.js" type="text/javascript"></script>
</body>
</html>
