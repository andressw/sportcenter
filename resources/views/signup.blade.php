@include('layouts.head_login_signup')
<form method="POST" action="{{ route('register') }}">
  {{ csrf_field() }}
<div class="large-12 columns no-padding contenidoFondoLoginSigup">
  <div class="large-4 medium-4 medium-offset-4 large-offset-4 no-padding columns contenidoLoginSigup">
    <div class="large-12 columns headerDivLogin">
      <h2>HTTP Response Monitor</h2>
      <span>Login Now</span>
    </div>
    <div class="larege-12 columns contentCenter">
      @if ($errors->has('name'))
          <span class="help-text" id="nameHelpText">
              {{ $errors->first('name') }}
          </span>
      @endif
      <input id="name" name="name" class="email_pass" type="text" value="{{ old('name') }}" placeholder="Your Name">

      @if ($errors->has('email'))
          <span class="help-text" id="emailHelpText">
              {{ $errors->first('email') }}
          </span>
      @endif
      <input id="email" name="email" class="email_pass" type="email" value="{{ old('email') }}" placeholder="Email">
      @if ($errors->has('password'))
          <span class="help-text">
              {{ $errors->first('password') }}
          </span>
      @endif
      <input id="password" name ="password" class="email_pass" type="password" placeholder="Password">
      <input id="password-confirm" name ="password_confirmation" class="email_pass" type="password" placeholder="Confirm Password">

      <input type="submit" class="button loginBtn" value="Sign up"></input>


    </div>
  </div>


</div>
</form>


@include('layouts.footer_login_signup')
