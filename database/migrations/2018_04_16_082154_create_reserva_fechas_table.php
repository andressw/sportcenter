<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservaFechasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('reserva_fechas', function (Blueprint $table) {
             $table->increments('id');
             $table->integer('idHora');
             $table->integer('idCancha');
             $table->string('nombre');
             $table->string('telefono');
             $table->string('comentario');
             $table->date('fechaDesdeNoJuega');
             $table->date('fechaHastaNoJuega');
             $table->boolean('jugo');
             $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('reserva_fechas');
     }
}
