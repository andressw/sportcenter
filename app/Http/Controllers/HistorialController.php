<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class HistorialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function darFechaEntreLunesDomingo($fecha)
    {
        $lunes = strtotime("last monday");
        $lunes = date('w', $lunes) == date('w') ? $lunes + 7 * 86400 : $lunes;

        $domingo = strtotime(date("Y-m-d", $lunes) . " +6 days");
        $flag = false;
        if ($fecha >= date("Y-m-d", $lunes) && $fecha <= date("Y-m-d", $domingo)) {
            $flag = true;
        }
        return $flag;
    }

    public function darHistorial($idCancha, $fechaDia = null)
    {
        if ($fechaDia == null) {
            $lunes = strtotime("last monday");
            $lunes = date('w', $lunes) == date('w') ? $lunes + 7 * 86400 : $lunes;
            $dia = strtotime(date("Y-m-d", $lunes) . " +" . (date("N") - 1) . " days");
            $fechaDia = date("Y-m-d", $dia);
        }
        $result = '';
        $result = DB::select("select * from (select historial.id as idH, `hora_dia`.`hora`, `hora_dia`.`id` as `idHora`, `historial`.`idReserva` as `idReserva`,`historial`.`idDia`, `historial`.`nombre`,  `historial`.`telefono`, `historial`.`comentario`, `historial`.`jugado`,`historial`.`soloHoy` from `hora_dia` left join `historial` on `hora_dia`.`id` = `historial`.`idHora` where (`historial`.`idCancha` = " . $idCancha . " and `historial`.`fecha` = '" . $fechaDia . "' ) UNION select 'idH' = null as 'idH', `hora_dia`.`hora`, `hora_dia`.`id` as `idHora`, 'idReserva' = null as 'idReserva','idDia' = null as 'idDia', 'nombre' = null as 'nombre', 'telefono' = null as 'telefono', 'comentario' = null as 'comentario', 'jugado' = null as 'jugado', 'soloHoy' = null as 'soloHoy' from hora_dia ) as t1 group by t1.idHora order by idHora");

        return ["fecha" => $fechaDia, "result" => $result];
    }

    public function cargarWeb()
    {
        //DB::enableQueryLog();
        $cancha = DB::table('canchas')->orderby("id")->get();
        $dias = DB::table('dia_semana')->orderby("id")->get();
        $horas = DB::table('hora_dia')->orderby("id")->get();
       
        $reservas = $this->darReserva($cancha[0]->id, $dias[date('N') - 1]->id);
        $fechaDiaMes = explode('-', $reservas["fecha"]);
        $canchaDia = $cancha[0]->nombre . " - " . $dias[date('N') - 1]->nombre;
        //dd(DB::getQueryLog());
        return view('reserva', ['seccion' => 'reservas', "fechaDiaMes" => $fechaDiaMes, "tituloSección" => 'Reservas', "fecha" => $reservas["fecha"], "idCancha" => $cancha[0]->id, "reservas" => $reservas["result"], "horas" => $horas, "canchas" => $cancha, "dias" => $dias, "canchaDia" => $canchaDia]);

    }
    public function cargarHistoralCancha(Request $request)
    {
        $cancha = DB::table('canchas')->where("id", $request->id)->get();
        $dias = DB::table('dia_semana')->orderby("id")->get();
        $diaSelected = date('N',strtotime($request->fecha));
        $reservas = $this->darHistorial($cancha[0]->id, $request->fecha);
        $fechaDiaMes = explode('-', $reservas["fecha"]);
        $canchaDia = $cancha[0]->nombre . " - " . $dias[$diaSelected - 1]->nombre . " <small>(" . $fechaDiaMes[2] . "/" . $fechaDiaMes[1] . ")</small>";

        $resultado = '';
        for ($i = 0; $i < count($reservas["result"]); $i++) {
            $resultado = ($reservas["result"][$i]->soloHoy = !null && $reservas["result"][$i]->soloHoy = !'' && $reservas["result"][$i]->soloHoy == 1) ? $resultado . '<tr class="soloDia">' : $resultado . '<tr>';
            $resultado = $resultado . '<td>' . substr($reservas["result"][$i]->hora, 0, -3) . '</td><td>' . $reservas["result"][$i]->nombre . '</td><td>' . $reservas["result"][$i]->telefono . '</td><td>' . $reservas["result"][$i]->comentario . '</td>';
            /**Aca genero el listado de las horas disponibles para el día ademas de las reservas*/
            if ($reservas["result"][$i]->idReserva != null) {
                if ($reservas["result"][$i]->jugado == 1) {
                    $resultado = $resultado . '<td>Si</td>';
                } else {
                    $resultado = $resultado . '<td>No</td>';
                }
                $resultado = $resultado . '<td><i class="fas fa-trash-alt" onclick="eliminarHistorial(' . $reservas["result"][$i]->idHora . ',' . $reservas["result"][$i]->idH . ');"></i><input type="hidden" name="idReserva' . $reservas["result"][$i]->idReserva . '" />';
                
            } else {
                $resultado = $resultado . '<td></td><td>';
                
            }
            $resultado = $resultado . '<input type="hidden" name="idTr' . $reservas["result"][$i]->idHora . '" /></td></tr>';

        }

        return response(["resultado" => $resultado, "canchaDia" => $canchaDia, "fecha" => $reservas["fecha"]], 200)
            ->header('Content-Type', 'json');

        //return view('reserva', ['seccion' => 'reservas', "tituloSección" => 'Reservas',"fecha" => $reservas["fecha"], "idCancha" => $cancha[0]->id, "reservas" => $reservas["result"],"horas" => $horas, "canchas" => $cancha, "dias" => $dias, "canchaDia"=> $canchaDia]);
    }

    

    public function cargaarVistaHistorial()
    {
        //DB::enableQueryLog();
        $cancha = DB::table('canchas')->orderby("id")->get();
        $dias = DB::table('dia_semana')->orderby("id")->get();
        $horas = DB::table('hora_dia')->orderby("id")->get();
       
        $canchaDia = $cancha[0]->nombre . " - " . $dias[date('N')-1]->nombre;
        $restul = $this->darHistorial($cancha[0]->id);
        $fechaDiaMes = explode('-', $restul["fecha"]);
        //dd(DB::getQueryLog());
        return view('historial_reservas', ['seccion' => 'historialReservas',"fechaDiaMes" => $fechaDiaMes,"canchaDia"=>$canchaDia, "reservas" => $restul["result"], "fecha" => $restul["fecha"], 'canchas' => $cancha, "idCancha" => $cancha[0]->id, "tituloSección" => 'Historial de reservas']);

    }

    public function eliminarHistorial(Request $request)
    {
        //DB::table('reservas')->where('id', $request->id)->delete();
        DB::table('historial')->where('id', $request->id)->delete();
    }

}
