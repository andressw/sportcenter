@include('layouts.head_panel')

<div class="contentCenter columns large-10 large-offset-2 medium-12 small-12">
  <div class="contentFormulario columns large-7 medium-12 small-12">
    @if (count($canchas) > 0)
    <div class="tituloTablaTr divComoTr table-expand-row columns large-12 medium-12 small-12">Nombre</div>
    @for ($i = 0; $i
    < count($canchas); $i++) <div id="trArriba{{$canchas[$i]->id}}" class="divComoTr table-expand-row columns large-12 medium-12 small-12"
      onclick="expandirOcultarContenido(this);">
      <span id="nombreId{{$canchas[$i]->id}}">{{$canchas[$i]->nombre}}</span>
      <span class="colapseExpandIcon">+</span>
  </div>


  <div id="trAbajo{{$canchas[$i]->id}}" class="divAbajoClass divComoTr table-expand-row-content columns large-12 medium-12 small-12">
    <div class="row rowModificarEliminar">
      <div class="columns large-6 medium-6 small-12 no-padding-left">
        <label class="labelNameInput">Nombre
          <input id="nombreCancha{{$canchas[$i]->id}}" type="text" placeholder="Nombre" value="{{$canchas[$i]->nombre}}">
        </label>
      </div>
      <div class="columns large-6 medium-6 small-12 no-padding-rigth">

          <label class="labelNameInput">Tipo de cancha
            <select id="selectTipoC{{$canchas[$i]->id}}">
              @for ($j = 0; $j
              < count($tipoC); $j++) <option {{($canchas[$i]->tipoCancha == $tipoC[$j]->id)?'selected':''}} value="{{$tipoC[$j]->id}}">{{$tipoC[$j]->nombre}}</option>
                @endfor
            </select>
          </label>
        </div>
        <div class="columns large-6 medium-6 small-12 no-padding-left">

          <label class="labelNameInput">Tipo de pasto
            <select id="selectTipoP{{$canchas[$i]->id}}">
              @for ($h = 0; $h
              < count($tipoP); $h++) <option {{($canchas[$i]->tipoPasto == $tipoP[$h]->id)?'selected':''}} value="{{$tipoP[$h]->id}}">{{$tipoP[$h]->nombre}}</option>
                @endfor
            </select>
          </label>
        </div>
        <div class="columns large-6 medium-6 small-12 no-padding-rigth">

          <label class="labelNameInput">Permite cumpleaños
            <select id="permiteCumple{{$canchas[$i]->id}}">
              <option {{($canchas[$i]->permiteCumpleanos == 0)?'selected':''}} value="0">No</option>
              <option {{($canchas[$i]->permiteCumpleanos == 1)?'selected':''}} value="1">Si</option>
            </select>
          </label>
        </div>
        <div class="columns large-12 medium-12 small-12 no-padding">
        <div class="columns large-6 medium-6 small-12 no-padding-left">

          <label class="labelNameInput">Precio
            <input id="precioCancha{{$canchas[$i]->id}}" type="number" placeholder="Precio" value="{{$canchas[$i]->precio}}">
          </label>
          </div></div>
        <div class="divContentSuccess columns large-6 medium-6 small-6">
        <button class="enviarAgregarListar buttonCustomSuccess button success" type="button" onclick="guardarModificarCancha({{$canchas[$i]->id}},this);">Guardar</button>
      </div>
      <div class="divContentAlert columns large-6 medium-6 small-6">
        <button class="enviarAgregarListar button alert" type="button" onclick="eliminarCancha({{$canchas[$i]->id}},this);">Eliminar</button>
      </div>

      
    </div>
  </div>
  

  @endfor
  @else
  <div class="noRegistrosIngresados columns large-12 medium-12 small-12">
    <span>No se encontraron registros ingresados.</span>
  </div>
  @endif
</div>
</div>
@include('layouts.footer_panel')