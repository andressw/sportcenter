<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\URL;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;
use Illuminate\Support\Facades\Hash;
use App\Sitios;
use App\User;

class SignupController extends Controller
{
  use RegistersUsers;
  function signupDeMonitoreo(Request $request){
    $pass_sin_hash = str_random(8);
    //$password_generated = Hash::make($pass_sin_hash);
    $emailUser = $request->emailUrlValue;
    $exist = DB::table('users')
      ->select(DB::raw('count(*) as user_count'))
      ->where('email', '=', $emailUser)
      ->get();
if($exist[0]->user_count == 0){
  $resUser = User::create([
    'name' => 'User',
    'email' => $emailUser,
    'password' => bcrypt($pass_sin_hash),
  ]);

  if($resUser){
    $sitio = new Sitios();
    $sitio->url = $request->textoUrlValue;
    $sitio->email_user = $emailUser;
    if(!$sitio->save()){
      return response('Error registering the url, please try again.', 500)
      ->header('Content-Type', 'text/plain');
    }else{
      $data = array(
        'email' => $emailUser,
        'pass' => $pass_sin_hash,
        'urlLogin' => url('/login')
      );
      Mail::send('emails.sigupMonitor', ['datos' => $data], function ($message) use ($emailUser) {
        $message->from('hola@hostingpedia.net', 'Contact from HTTP Response Monitor');
        $message->to($emailUser)->subject('Contact from HTTP Response Monitor');
      });
      return response('Great! You have been a successful register, we send you an email with the password to enter on the panel.', 200)
      ->header('Content-Type', 'text/plain');
    }

  }else{
    return response('Error, please try again.', 500)
    ->header('Content-Type', 'text/plain');
  }
}else{
  return response('Error, the email account exist, please try again with diferent email account.', 500)
  ->header('Content-Type', 'text/plain');
}


  }

  function resetPassword(Request $request){
    $email_sended = $request->emailReset;
    $users = DB::table('users')
      ->select(DB::raw('count(*) as user_count'))
      ->where('email', '=', $email_sended)
      ->get();
    if($users[0]->user_count > 0){
      $pass_sin_hash = str_random(8);
      DB::table('users')
        ->where('email', $email_sended)
        ->update(['password' => bcrypt($pass_sin_hash)]);
      $data = array(
        'email' => $email_sended,
        'pass' => $pass_sin_hash,
          'urlLogin' => url('/login')
      );
      Mail::send('emails.resetPass', ['datos' => $data], function ($message) use ($email_sended) {
        $message->from('hola@hostingpedia.net', 'Password Reset from HTTP Response Monitor');
        $message->to($email_sended)->subject('Password Reset from HTTP Response Monitor');
      });
    }else{
      return response('Invalid Email.', 500)
      ->header('Content-Type', 'text/plain');
    }



  }


}
