-- MySQL dump 10.13  Distrib 5.5.58, for Linux (x86_64)
--
-- Host: localhost    Database: sportcen_db_new
-- ------------------------------------------------------
-- Server version	5.5.58

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `canchas`
--

DROP TABLE IF EXISTS `canchas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `canchas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipoCancha` int(11) NOT NULL,
  `tipoPasto` int(11) NOT NULL,
  `permiteCumpleanos` tinyint(1) NOT NULL,
  `precio` int(11) NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `canchas`
--

LOCK TABLES `canchas` WRITE;
/*!40000 ALTER TABLE `canchas` DISABLE KEYS */;
INSERT INTO `canchas` VALUES (1,'Futbol 5',7,4,1,400,'url nueva','2018-06-25 22:27:24','2018-06-25 22:27:24'),(5,'Futboll 11',7,4,1,400,NULL,'2018-06-26 00:57:52','2018-06-26 00:57:52'),(6,'Futboll 9 1',7,4,0,345,NULL,'2018-07-12 08:27:32','2018-07-12 08:27:32'),(7,'Futboll 9 2',7,4,0,22,NULL,'2018-07-12 08:27:40','2018-07-12 08:27:40');
/*!40000 ALTER TABLE `canchas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dia_semana`
--

DROP TABLE IF EXISTS `dia_semana`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dia_semana` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dia_semana`
--

LOCK TABLES `dia_semana` WRITE;
/*!40000 ALTER TABLE `dia_semana` DISABLE KEYS */;
INSERT INTO `dia_semana` VALUES (1,'Lunes'),(2,'Martes'),(3,'Miércoles'),(4,'Jueves'),(5,'Viernes'),(6,'Sábado'),(7,'Domingo');
/*!40000 ALTER TABLE `dia_semana` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historial`
--

DROP TABLE IF EXISTS `historial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historial` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idReserva` int(11) NOT NULL,
  `idHora` int(11) NOT NULL,
  `idDia` int(11) NOT NULL,
  `idCancha` int(11) NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comentario` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` date NOT NULL,
  `jugado` tinyint(1) NOT NULL,
  `soloHoy` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historial`
--

LOCK TABLES `historial` WRITE;
/*!40000 ALTER TABLE `historial` DISABLE KEYS */;
INSERT INTO `historial` VALUES (1,1,4,3,1,'Pedro','12312312','asdsadasdasdas','2018-08-06',1,1,NULL,NULL),(3,3,9,4,5,'prueba','213','ads','2018-08-06',0,1,NULL,NULL),(4,4,4,4,6,'qqqqq','1111','aaaa','2018-08-06',0,1,NULL,NULL),(5,5,9,4,6,'Andrés Schiaffarino','111',NULL,'2018-08-06',0,1,NULL,NULL),(6,6,1,4,6,'Nueva prueba','098542577',NULL,'2018-08-06',0,1,NULL,NULL),(7,7,2,4,1,'prueba agregado','111111111111','22222222222','2018-08-06',0,1,NULL,NULL),(8,8,3,4,1,'segunda','132456798','comentario','2018-08-06',0,1,NULL,NULL),(11,11,13,4,1,'asd','asd','asd','2018-08-03',0,1,NULL,NULL),(12,12,5,1,1,'Prueba','1111','1111','2018-08-03',0,1,NULL,NULL),(13,13,7,1,1,'qq','qq','qq','2018-08-03',0,1,NULL,NULL),(14,14,11,4,1,'aa','aa','aa','2018-08-03',0,1,NULL,NULL),(15,15,21,4,1,'aa','aa','aa','2018-08-03',0,1,NULL,NULL),(16,16,1,4,8,'aa','aa',NULL,'2018-08-03',0,1,NULL,NULL),(17,17,5,4,7,'a','a',NULL,'2018-08-03',0,1,NULL,NULL),(18,18,2,4,1,'aaa','aaa',NULL,'2018-08-03',0,1,NULL,NULL),(19,19,1,4,10,'aa','aa','aa','2018-08-03',0,1,NULL,NULL),(20,20,2,7,1,'p','p','p','2018-08-03',0,1,NULL,NULL),(21,21,18,7,1,'sss','ss',NULL,'2018-08-03',0,1,NULL,NULL),(22,22,2,5,1,'aaa','123','asd','2018-08-03',0,1,NULL,NULL),(23,24,4,3,1,'aaaaaaaaaaaaa','aaaaaaa',NULL,'2018-08-03',0,1,NULL,NULL),(24,26,7,1,1,'aaa','aaaa',NULL,'2018-08-03',0,1,NULL,NULL),(25,27,11,2,1,'aaaa','1qq',NULL,'2018-08-03',0,1,NULL,NULL),(26,29,4,1,1,'eeee','qqq','aadas','2018-08-03',0,1,NULL,NULL),(27,30,8,6,1,'asd','asd','asd','2018-08-03',0,1,NULL,NULL),(28,31,4,3,7,'aaaa','aaa',NULL,'2018-08-03',0,1,NULL,NULL),(29,32,7,2,1,'aa','aa',NULL,'2018-08-03',0,1,NULL,NULL),(31,35,14,2,1,'11','11',NULL,'2018-08-01',0,1,NULL,NULL),(32,36,16,2,1,'22','22',NULL,'2018-08-01',0,1,NULL,NULL),(33,37,19,2,1,'333','444',NULL,'2018-08-01',0,1,NULL,NULL),(34,38,21,2,1,'qq','qq',NULL,'2018-08-01',0,1,NULL,NULL),(35,39,5,2,1,'asaas','aas',NULL,'2018-08-01',0,1,NULL,NULL),(36,40,14,2,1,'aa','aa',NULL,'2018-08-01',0,1,NULL,NULL),(37,41,8,3,1,'prueba','1111','aaaa','2018-08-01',0,1,NULL,NULL),(38,42,10,3,1,'preuba 2','12','12','2018-08-01',0,1,NULL,NULL),(39,44,14,3,1,'Prueba 2 semanal','Prueba 2 semanal','Prueba 2 semanal','2018-08-01',0,0,NULL,NULL),(40,45,16,3,1,'Prueba 3 semanal','Prueba 3 semanal','Prueba 3 semanal','2018-08-01',0,0,NULL,NULL),(41,48,1,3,1,'asd11','asd11','asd11','2018-08-01',1,1,NULL,NULL),(42,50,7,3,1,'asd','asd','asd','2018-08-01',0,1,NULL,NULL),(43,51,3,3,5,'asd','asd',NULL,'2018-08-01',1,1,NULL,NULL),(44,52,7,4,1,'Andrés Schiaffarino','099930883','Juega hoy a la tarde','2018-08-01',1,1,NULL,NULL),(45,54,11,6,1,'Prueba real','Prueba real','Prueba real','2018-08-01',0,0,NULL,NULL),(46,55,2,3,1,'Prueba deshabilitado','Prueba deshabilitado','Prueba deshabilitado','2018-08-01',0,0,NULL,NULL),(47,63,5,3,7,'asd','asd','asd','2018-08-01',0,1,NULL,NULL),(48,64,7,3,7,'asd','asd','asd','2018-08-01',0,1,NULL,NULL),(49,65,1,3,7,'asd','asd','asd','2018-08-01',0,1,NULL,NULL),(50,66,6,3,1,'asd','asd',NULL,'2018-08-06',0,1,NULL,NULL),(51,77,6,4,1,'asd','asd',NULL,'2018-08-02',0,1,NULL,NULL),(52,99,15,4,6,'aaaaaaaaaaaaaaa','qqqqqqqqqqqqqq','111111111111111','2018-08-02',0,1,NULL,NULL),(53,105,14,4,1,'Andrés Schiaffarino','099930883','asd','2018-08-02',0,1,NULL,NULL),(54,106,5,2,1,'Andrés Schiaffarino','099930883',NULL,'2018-08-02',1,1,NULL,NULL),(55,108,12,2,1,'asd','asd',NULL,'2018-08-02',1,1,NULL,NULL),(56,112,21,2,1,'asd','asd','asd','2018-08-02',1,1,NULL,NULL),(57,113,19,2,1,'sad','asd',NULL,'2018-08-02',0,1,NULL,NULL),(58,114,8,2,1,'asd','asd',NULL,'2018-08-02',0,1,NULL,NULL),(59,115,23,3,1,'asd','asd','asd','2018-08-02',0,1,NULL,NULL),(60,116,24,2,1,'asd','asd','asd','2018-08-02',1,1,NULL,NULL),(61,117,4,3,1,'Andrés Schiaffarino','099930883','asd','2018-08-02',1,1,NULL,NULL),(62,119,7,3,1,'asd','asd','asd','2018-08-02',1,1,NULL,NULL),(63,120,10,3,1,'asd','asd','asd','2018-08-02',1,1,NULL,NULL),(64,121,14,2,1,'Andrés Schiaffarino','099930883','asd','2018-08-02',0,1,NULL,NULL),(65,122,16,2,1,'Andrés Schiaffarino','099930883','asd','2018-08-02',0,1,NULL,NULL),(66,123,1,3,5,'Andrés Schiaffarino1','132123a','asdas1','2018-08-02',1,0,NULL,NULL),(67,124,2,3,5,'ggg','111','aaa','2018-08-02',1,1,NULL,NULL),(68,125,1,6,1,'asd','asd','asd','2018-08-02',0,1,NULL,NULL),(69,126,20,3,1,'asd','asd','asd','2018-08-02',0,1,NULL,NULL),(71,128,5,3,1,'asd','asd','asd','2018-08-02',0,0,NULL,NULL),(72,129,24,3,1,'asd','asd','asd','2018-08-02',0,1,NULL,NULL),(73,130,6,3,1,'asd','asd','asd','2018-08-02',0,0,NULL,NULL),(74,132,18,3,1,'asd','asd','asd','2018-08-02',0,0,NULL,NULL),(75,133,9,3,1,'asd','asd','asd','2018-08-02',0,0,NULL,NULL),(76,134,17,3,1,'asd','asd','asd','2018-08-02',0,0,NULL,NULL),(77,135,15,3,1,'asd','asd','asd','2018-08-02',0,0,NULL,NULL),(78,137,8,4,1,'asd','asd','asd','2018-08-02',1,0,NULL,NULL),(79,139,12,4,1,'asd','asd','asd','2018-08-02',1,0,NULL,NULL),(81,141,16,4,1,'asd','asd','asd','2018-07-28',1,0,NULL,NULL),(82,142,1,4,1,'asd','asd','asd','2018-07-28',0,1,NULL,NULL),(83,143,1,4,5,'asd','asd','asd','2018-07-28',1,0,NULL,NULL),(84,144,4,4,1,'Andrés Schiaffarino','099930883','asd','2018-07-28',1,0,NULL,NULL),(85,145,1,4,6,'asd','asd','asd','2018-07-28',0,0,NULL,NULL),(86,146,4,4,6,'asd','asd','asd','2018-07-28',0,1,NULL,NULL),(87,147,1,4,6,'asd','asd','asd','2018-07-28',0,1,NULL,NULL),(88,148,5,4,6,'asd','asd','asd','2018-07-28',0,1,NULL,NULL),(89,149,3,6,1,'asd','asd','asd','2018-07-28',1,1,NULL,NULL),(90,150,1,7,1,'Prueba 1 Futbol 5','Prueba1Futbol5','Prueba 1 Futbol 5','2018-07-28',1,1,NULL,NULL),(91,151,2,7,1,'Prueba 2 Futbol 5','Prueba2Futbol5','Prueba 2 Futbol 5','2018-07-28',1,1,NULL,NULL),(92,152,6,7,1,'Prueba 3 Futbol 5','Prueba3Futbol5','Prueba 3 Futbol 5','2018-07-28',0,0,NULL,NULL),(93,153,1,7,5,'Prueba 1 Futbol 11','Prueba1Futbol11','Prueba 1 Futbol 11','2018-07-28',0,1,NULL,NULL),(94,154,6,7,5,'Prueba 2 Futbol 11','Prueba2Futbol11','Prueba 2 Futbol 11','2018-07-28',0,0,NULL,NULL),(95,155,3,7,6,'Prueba 1 Futbol 9 1','Prueba1Futbol91','Prueba 1 Futbol 9 1','2018-07-28',1,1,NULL,NULL),(96,156,6,7,6,'Prueba 2 Futbol 9 1','Prueba2Futbol91','Prueba 2 Futbol 9 1','2018-07-28',0,1,NULL,NULL),(97,157,4,7,7,'Prueba 1 Futbol 9 2','Prueba1Futbol91','Prueba 1 Futbol 9 1','2018-07-28',0,1,NULL,NULL),(98,158,14,7,8,'Prueba 1 Futbol 7','Prueba1Futbol7','Prueba 1 Futbol 7','2018-07-28',1,1,NULL,NULL),(99,159,3,7,10,'Prueba 1 Futbol 12','Prueba1Futbol12','Prueba 1 Futbol 12','2018-07-28',1,1,NULL,NULL),(100,160,1,1,1,'lunes','lunes','lunes','2018-07-28',0,1,NULL,NULL),(101,161,2,1,1,'lunes','lunes','lunes','2018-07-28',0,0,NULL,NULL),(102,162,5,1,5,'lunes','lunes','lunes','2018-07-28',0,1,NULL,NULL),(103,163,9,1,5,'lunes','lunes','lunes','2018-07-28',0,1,NULL,NULL),(104,164,4,1,6,'lunes','lunes','lunes','2018-07-28',0,1,NULL,NULL),(105,44,14,3,1,'Prueba 2 semanal','Prueba 2 semanal','Prueba 2 semanal','2018-09-05',0,0,NULL,NULL),(106,45,16,3,1,'Prueba 3 semanal','Prueba 3 semanal','Prueba 3 semanal','2018-09-05',0,0,NULL,NULL),(107,123,1,3,5,'Andrés Schiaffarino1','132123a','asdas1','2018-09-05',1,0,NULL,NULL),(108,128,5,3,1,'asd','asd','asd','2018-09-05',1,0,NULL,NULL),(110,132,18,3,1,'asd','asd','asd','2018-09-05',0,0,NULL,NULL),(111,133,9,3,1,'asd','asd','asd','2018-09-05',0,0,NULL,NULL),(112,134,17,3,1,'asd','asd','asd','2018-09-05',0,0,NULL,NULL),(113,135,15,3,1,'asd','asd','asd','2018-09-05',0,0,NULL,NULL);
/*!40000 ALTER TABLE `historial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hora_dia`
--

DROP TABLE IF EXISTS `hora_dia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hora_dia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hora` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hora_dia`
--

LOCK TABLES `hora_dia` WRITE;
/*!40000 ALTER TABLE `hora_dia` DISABLE KEYS */;
INSERT INTO `hora_dia` VALUES (1,'00:00:00'),(2,'01:00:00'),(3,'02:00:00'),(4,'03:00:00'),(5,'04:00:00'),(6,'05:00:00'),(7,'06:00:00'),(8,'07:00:00'),(9,'08:00:00'),(10,'09:00:00'),(11,'10:00:00'),(12,'11:00:00'),(13,'12:00:00'),(14,'13:00:00'),(15,'14:00:00'),(16,'15:00:00'),(17,'16:00:00'),(18,'17:00:00'),(19,'18:00:00'),(20,'19:00:00'),(21,'20:00:00'),(22,'21:00:00'),(23,'22:00:00'),(24,'23:00:00');
/*!40000 ALTER TABLE `hora_dia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lista_negras`
--

DROP TABLE IF EXISTS `lista_negras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lista_negras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comentario` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lista_negras`
--

LOCK TABLES `lista_negras` WRITE;
/*!40000 ALTER TABLE `lista_negras` DISABLE KEYS */;
INSERT INTO `lista_negras` VALUES (31,'Jorge','099854213','Reservó dos veces y faltó sin avisar nada incluso luego de haber confirmado que iba.','2018-07-10 08:52:47','2018-07-10 08:52:47'),(32,'Pedro','099999999','Se llevaron una pechera y no vinieron más, hablé con el encargado de los chicos y el lunes viene a pagarla','2018-07-10 09:02:28','2018-07-10 09:02:28'),(66,'asd','aaaaaaaaaaaaaaaa','aaaaaaaaaaaa','2018-07-15 06:26:49','2018-07-15 06:26:49'),(67,'Diego11',' - ','-','2018-07-17 09:40:10','2018-07-17 09:40:10'),(69,'Prueba de agregado1','0998542561','No paga nuncaa11','2018-07-23 07:13:44','2018-07-23 07:13:44'),(70,'neuvo','nuev','nuevo','2018-07-23 07:19:46','2018-07-23 07:19:46'),(71,'nueva prueba','nueva prueba','nueva prueba','2018-07-23 07:23:00','2018-07-23 07:23:00'),(72,'nueva prueba','nueva prueba','nueva prueba','2018-07-23 07:23:23','2018-07-23 07:23:23'),(73,'qqqqqqqqqqqqqqqqqq','qqqqqqqqqqqqq','qqqqqqqqqqqqqq','2018-07-23 07:25:19','2018-07-23 07:25:19'),(74,'Andrés','099930883','No paga','2018-07-26 09:32:08','2018-07-26 09:32:08');
/*!40000 ALTER TABLE `lista_negras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (2,'2014_10_12_100000_create_password_resets_table',1),(4,'2018_04_16_062247_create_canchas_table',1),(5,'2018_04_16_062253_create_servicios_table',1),(7,'2018_04_16_062825_create_tipo_canchas_table',1),(8,'2018_04_16_062831_create_tipo_pastos_table',1),(9,'2018_04_16_062841_create_tipo_servicios_table',1),(10,'2018_04_16_064334_dia_semana',1),(11,'2018_04_16_064542_hora_dia',1),(12,'2018_04_16_082154_create_reserva_fechas_table',1),(13,'2018_04_16_082222_reservas_dia',1),(14,'2014_10_12_000000_create_users_table',2),(17,'2018_04_16_061209_create_lista_negras_table',3),(19,'2018_04_16_062316_create_reservas_table',4),(20,'2018_08_02_071110_historial',5);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reserva_fechas`
--

DROP TABLE IF EXISTS `reserva_fechas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reserva_fechas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idHora` int(11) NOT NULL,
  `idCancha` int(11) NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comentario` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fechaDesdeNoJuega` date NOT NULL,
  `fechaHastaNoJuega` date NOT NULL,
  `jugo` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reserva_fechas`
--

LOCK TABLES `reserva_fechas` WRITE;
/*!40000 ALTER TABLE `reserva_fechas` DISABLE KEYS */;
/*!40000 ALTER TABLE `reserva_fechas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservas`
--

DROP TABLE IF EXISTS `reservas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idHora` int(11) NOT NULL,
  `idDia` int(11) NOT NULL,
  `idCancha` int(11) NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comentario` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fechaDesdeNoJuega` date DEFAULT NULL,
  `fechaHastaNoJuega` date DEFAULT NULL,
  `jugado` tinyint(1) NOT NULL,
  `soloHoy` tinyint(1) NOT NULL,
  `hoyJuega` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservas`
--

LOCK TABLES `reservas` WRITE;
/*!40000 ALTER TABLE `reservas` DISABLE KEYS */;
INSERT INTO `reservas` VALUES (1,4,3,1,'Pedro','12312312','asdsadasdasdas','2018-07-18',NULL,NULL,0,1,1,NULL,NULL),(3,9,4,5,'prueba','213','ads','2018-07-19',NULL,NULL,0,1,1,'2018-07-19 05:27:42','2018-07-19 05:27:42'),(4,4,4,6,'qqqqq','1111','aaaa','2018-07-19',NULL,NULL,0,1,1,'2018-07-19 06:26:24','2018-07-19 06:26:24'),(5,9,4,6,'Andrés Schiaffarino','111',NULL,'2018-07-19',NULL,NULL,0,1,1,'2018-07-19 06:39:44','2018-07-19 06:39:44'),(6,1,4,6,'Nueva prueba','098542577',NULL,'2018-07-19',NULL,NULL,0,1,1,'2018-07-19 06:45:26','2018-07-19 06:45:26'),(7,2,4,1,'prueba agregado','111111111111','22222222222','2018-07-19',NULL,NULL,0,1,1,'2018-07-19 07:07:36','2018-07-19 07:07:36'),(8,3,4,1,'segunda','132456798','comentario','2018-07-19',NULL,NULL,0,1,1,'2018-07-19 07:12:10','2018-07-19 07:12:10'),(11,13,4,1,'asd','asd','asd','2018-07-19',NULL,NULL,0,1,1,'2018-07-19 07:15:23','2018-07-19 07:15:23'),(12,5,1,1,'Prueba','1111','1111','2018-07-16',NULL,NULL,0,1,1,'2018-07-19 09:12:22','2018-07-19 09:12:22'),(13,7,1,1,'qq','qq','qq','2018-07-16',NULL,NULL,0,1,1,'2018-07-19 09:12:46','2018-07-19 09:12:46'),(14,11,4,1,'aa','aa','aa','2018-07-19',NULL,NULL,0,1,1,'2018-07-19 09:13:46','2018-07-19 09:13:46'),(15,21,4,1,'aa','aa','aa','2018-07-19',NULL,NULL,0,1,1,'2018-07-19 09:14:40','2018-07-19 09:14:40'),(17,5,4,7,'a','a',NULL,'2018-07-19',NULL,NULL,0,1,1,'2018-07-19 09:15:27','2018-07-19 09:15:27'),(18,2,4,1,'aaa','aaa',NULL,'2018-07-19',NULL,NULL,0,1,1,'2018-07-19 09:20:42','2018-07-19 09:20:42'),(20,2,7,1,'p','p','p','2018-07-22',NULL,NULL,0,1,1,'2018-07-22 05:36:55','2018-07-22 05:36:55'),(21,18,7,1,'sss','ss',NULL,'2018-07-22',NULL,NULL,0,1,1,'2018-07-22 09:52:51','2018-07-22 09:52:51'),(22,2,5,1,'aaa','123','asd','2018-08-03',NULL,NULL,0,1,1,'2018-07-23 04:24:38','2018-07-23 04:24:38'),(24,4,3,1,'aaaaaaaaaaaaa','aaaaaaa',NULL,'2018-07-25',NULL,NULL,0,1,1,'2018-07-23 04:42:23','2018-07-23 04:42:23'),(26,7,1,1,'aaa','aaaa',NULL,'2018-07-30',NULL,NULL,0,1,1,'2018-07-23 04:43:58','2018-07-23 04:43:58'),(27,11,2,1,'aaaa','1qq',NULL,'2018-07-24',NULL,NULL,0,1,1,'2018-07-23 04:49:26','2018-07-23 04:49:26'),(29,4,1,1,'eeee','qqq','aadas','2018-07-30',NULL,NULL,0,1,1,'2018-07-23 04:56:42','2018-07-23 04:56:42'),(30,8,6,1,'asd','asd','asd','2018-07-28',NULL,NULL,0,1,1,'2018-07-23 04:57:04','2018-07-23 04:57:04'),(31,4,3,7,'aaaa','aaa',NULL,'2018-07-25',NULL,NULL,0,1,1,'2018-07-23 05:41:23','2018-07-23 05:41:23'),(32,7,2,1,'aa','aa',NULL,'2018-07-24',NULL,NULL,0,1,1,'2018-07-23 06:05:38','2018-07-23 06:05:38'),(35,14,2,1,'11','11',NULL,'2018-07-24',NULL,NULL,0,1,1,'2018-07-23 06:08:00','2018-07-23 06:08:00'),(36,16,2,1,'22','22',NULL,'2018-07-24',NULL,NULL,0,1,1,'2018-07-23 06:09:05','2018-07-23 06:09:05'),(37,19,2,1,'333','444',NULL,'2018-07-24',NULL,NULL,0,1,1,'2018-07-23 06:09:36','2018-07-23 06:09:36'),(38,21,2,1,'qq','qq',NULL,'2018-07-24',NULL,NULL,0,1,1,'2018-07-23 06:16:33','2018-07-23 06:16:33'),(39,5,2,1,'asaas','aas',NULL,'2018-07-24',NULL,NULL,0,1,1,'2018-07-23 06:19:45','2018-07-23 06:19:45'),(40,14,2,1,'aa','aa',NULL,'2018-07-24',NULL,NULL,0,1,1,'2018-07-23 06:21:36','2018-07-23 06:21:36'),(41,8,3,1,'prueba','1111','aaaa','2018-07-25','0000-00-00','0000-00-00',0,1,1,'2018-07-25 04:32:52','2018-07-25 04:32:52'),(42,10,3,1,'preuba 2','12','12','2018-07-25','0000-00-00','0000-00-00',0,1,1,'2018-07-25 04:34:08','2018-07-25 04:34:08'),(44,14,3,1,'Prueba 2 semanal','Prueba 2 semanal','Prueba 2 semanal','2018-07-25','0000-00-00','0000-00-00',0,0,1,'2018-07-25 04:36:22','2018-07-25 04:36:22'),(45,16,3,1,'Prueba 3 semanal','Prueba 3 semanal','Prueba 3 semanal','2018-07-25','0000-00-00','0000-00-00',0,0,1,'2018-07-25 04:37:06','2018-07-25 04:37:06'),(48,1,3,1,'asd11','asd11','asd11','2018-08-01','0000-00-00','0000-00-00',1,1,1,'2018-07-25 06:09:10','2018-08-01 08:56:27'),(50,7,3,1,'asd','asd','asd','2018-07-25','0000-00-00','0000-00-00',0,1,1,'2018-07-25 06:11:01','2018-07-25 06:11:01'),(51,3,3,5,'asd','asd',NULL,'2018-08-01','0000-00-00','0000-00-00',1,1,1,'2018-07-25 06:11:36','2018-08-01 10:02:52'),(52,7,4,1,'Andrés Schiaffarino','099930883','Juega hoy a la tarde','2018-08-02','0000-00-00','0000-00-00',1,1,1,'2018-07-25 06:12:19','2018-08-02 06:43:20'),(54,11,6,1,'Prueba real','Prueba real','Prueba real','2018-07-28','2018-08-25','2018-09-08',0,0,1,'2018-07-25 06:16:55','2018-07-25 06:16:55'),(55,2,3,1,'Prueba deshabilitado','Prueba deshabilitado','Prueba deshabilitado','2018-07-25','2018-07-27','2018-09-20',0,0,1,'2018-07-25 06:53:42','2018-07-25 06:53:42'),(63,5,3,7,'asd','asd','asd','2018-07-25','0000-00-00','0000-00-00',0,1,1,'2018-07-25 09:38:17','2018-07-25 09:38:17'),(64,7,3,7,'asd','asd','asd','2018-07-25','0000-00-00','0000-00-00',0,1,1,'2018-07-25 09:38:37','2018-07-25 09:38:37'),(65,1,3,7,'asd','asd','asd','2018-07-25','0000-00-00','0000-00-00',0,1,1,'2018-07-25 09:39:05','2018-07-25 09:39:05'),(66,6,3,1,'asd','asd',NULL,'2018-07-25','0000-00-00','0000-00-00',0,1,1,'2018-07-26 05:01:08','2018-07-26 05:01:08'),(77,6,4,1,'asd','asd',NULL,'2018-08-09','0000-00-00','0000-00-00',0,1,1,'2018-07-26 05:42:21','2018-07-26 05:42:21'),(99,15,4,6,'aaaaaaaaaaaaaaa','qqqqqqqqqqqqqq','111111111111111','2018-07-26','0000-00-00','0000-00-00',0,1,1,'2018-07-26 07:48:53','2018-07-26 07:48:53'),(105,14,4,1,'Andrés Schiaffarino','099930883','asd','2018-07-26','0000-00-00','0000-00-00',0,1,1,'2018-07-26 09:43:54','2018-07-26 09:43:54'),(106,5,2,1,'Andrés Schiaffarino','099930883',NULL,'2018-07-31','0000-00-00','0000-00-00',1,1,1,'2018-07-31 03:39:08','2018-07-31 03:39:08'),(108,12,2,1,'asd','asd',NULL,'2018-07-31','0000-00-00','0000-00-00',1,1,1,'2018-07-31 04:31:41','2018-07-31 04:31:41'),(112,21,2,1,'asd','asd','asd','2018-07-31','0000-00-00','0000-00-00',1,1,1,'2018-07-31 04:35:57','2018-07-31 04:35:57'),(113,19,2,1,'sad','asd',NULL,'2018-07-31','0000-00-00','0000-00-00',0,1,1,'2018-07-31 04:38:38','2018-07-31 04:38:38'),(114,8,2,1,'asd','asd',NULL,'2018-07-31','0000-00-00','0000-00-00',0,1,1,'2018-07-31 04:46:25','2018-07-31 04:46:25'),(115,23,3,1,'asd','asd','asd','2018-08-01','0000-00-00','0000-00-00',0,1,1,'2018-07-31 04:46:55','2018-07-31 04:46:55'),(116,24,2,1,'asd','asd','asd','2018-07-31','0000-00-00','0000-00-00',1,1,1,'2018-07-31 04:50:38','2018-07-31 04:50:38'),(117,4,3,1,'Andrés Schiaffarino','099930883','asd','2018-08-01','0000-00-00','0000-00-00',1,1,1,'2018-07-31 04:59:15','2018-08-01 09:05:51'),(119,7,3,1,'asd','asd','asd','2018-08-01','0000-00-00','0000-00-00',1,1,1,'2018-07-31 05:10:12','2018-08-01 09:12:49'),(120,10,3,1,'asd','asd','asd','2018-08-01','0000-00-00','0000-00-00',1,1,1,'2018-07-31 05:10:28','2018-08-01 10:03:05'),(121,14,2,1,'Andrés Schiaffarino','099930883','asd','2018-07-31','0000-00-00','0000-00-00',0,1,1,'2018-07-31 08:43:29','2018-07-31 08:43:29'),(122,16,2,1,'Andrés Schiaffarino','099930883','asd','2018-07-31','0000-00-00','0000-00-00',0,1,1,'2018-07-31 09:01:10','2018-07-31 09:01:10'),(123,1,3,5,'Andrés Schiaffarino1','132123a','asdas1','2018-08-01','2018-08-03','2018-08-05',1,0,1,'2018-07-31 09:12:12','2018-07-31 09:12:12'),(124,2,3,5,'ggg','111','aaa','2018-08-01','0000-00-00','0000-00-00',1,1,1,'2018-08-01 09:34:19','2018-08-01 10:00:07'),(125,1,6,1,'asd','asd','asd','2018-08-04','0000-00-00','0000-00-00',0,1,1,'2018-08-01 09:34:33','2018-08-04 18:59:08'),(126,20,3,1,'asd','asd','asd','2018-08-01','0000-00-00','0000-00-00',0,1,1,'2018-08-01 09:34:45','2018-08-01 09:34:45'),(128,5,3,1,'asd','asd','asd','2018-08-01','2018-08-04','2018-08-16',1,0,1,'2018-08-01 09:35:36','2018-08-01 09:35:36'),(129,24,3,1,'asd','asd','asd','2018-08-01','0000-00-00','0000-00-00',0,1,1,'2018-08-01 09:36:43','2018-08-01 09:36:43'),(130,6,3,1,'asd','asd','asd','2018-08-01','0000-00-00','0000-00-00',0,0,1,'2018-08-01 09:36:55','2018-08-01 09:36:55'),(132,18,3,1,'asd','asd','asd','2018-08-01','0000-00-00','0000-00-00',0,0,1,'2018-08-01 09:39:29','2018-08-01 09:39:29'),(133,9,3,1,'asd','asd','asd','2018-08-01','0000-00-00','0000-00-00',1,0,1,'2018-08-01 09:46:29','2018-08-01 09:46:29'),(134,17,3,1,'asd','asd','asd','2018-08-01','0000-00-00','0000-00-00',0,0,1,'2018-08-01 09:46:59','2018-08-01 09:46:59'),(135,15,3,1,'asd','asd','asd','2018-08-01','2018-08-03','2018-08-05',0,0,1,'2018-08-01 09:47:25','2018-08-01 09:47:25'),(137,8,4,1,'asd','asd','asd','2018-08-02','0000-00-00','0000-00-00',1,0,1,'2018-08-02 06:08:02','2018-08-02 06:42:50'),(139,12,4,1,'asd','asd','asd','2018-08-02','0000-00-00','0000-00-00',1,0,1,'2018-08-02 06:13:09','2018-08-02 06:43:09'),(141,16,4,1,'asd','asd','asd','2018-08-02','2018-08-09','2018-08-09',1,0,1,'2018-08-02 06:43:27','2018-08-02 06:46:03'),(142,1,4,1,'asd','asd','asd','2018-08-09','0000-00-00','0000-00-00',0,1,1,'2018-08-02 06:48:07','2018-08-02 06:48:07'),(143,1,4,5,'asd','asd','asd','2018-08-02','2018-08-09','2018-08-09',1,0,1,'2018-08-02 06:48:49','2018-08-02 07:06:33'),(144,4,4,1,'Andrés Schiaffarino','099930883','asd','2018-08-02','0000-00-00','0000-00-00',1,0,1,'2018-08-02 06:52:32','2018-08-02 07:07:12'),(145,1,4,6,'asd','asd','asd','2018-08-02','2018-08-09','2018-08-16',0,0,1,'2018-08-02 07:42:30','2018-08-02 07:45:15'),(146,4,4,6,'asd','asd','asd','2018-08-02','0000-00-00','0000-00-00',0,1,1,'2018-08-02 07:42:47','2018-08-02 07:42:47'),(147,1,4,6,'asd','asd','asd','2018-08-09','0000-00-00','0000-00-00',0,1,1,'2018-08-02 07:43:22','2018-08-02 07:43:22'),(148,5,4,6,'asd','asd','asd','2018-08-09','0000-00-00','0000-00-00',0,1,1,'2018-08-02 07:44:47','2018-08-02 07:44:47'),(149,3,6,1,'asd','asd','asd','2018-08-11','0000-00-00','0000-00-00',1,1,1,'2018-08-05 00:09:24','2018-08-05 00:09:52'),(150,1,7,1,'Prueba 1 Futbol 5','Prueba1Futbol5','Prueba 1 Futbol 5','2018-08-05','0000-00-00','0000-00-00',1,1,1,'2018-08-05 20:43:55','2018-08-05 20:43:55'),(151,2,7,1,'Prueba 2 Futbol 5','Prueba2Futbol5','Prueba 2 Futbol 5','2018-08-05','0000-00-00','0000-00-00',1,1,1,'2018-08-05 20:44:11','2018-08-05 20:44:11'),(152,6,7,1,'Prueba 3 Futbol 5','Prueba3Futbol5','Prueba 3 Futbol 5','2018-08-05','2018-08-12','2018-08-19',0,0,1,'2018-08-05 20:44:41','2018-08-05 20:44:41'),(153,1,7,5,'Prueba 1 Futbol 11','Prueba1Futbol11','Prueba 1 Futbol 11','2018-08-05','0000-00-00','0000-00-00',0,1,1,'2018-08-05 20:45:06','2018-08-05 20:45:06'),(154,6,7,5,'Prueba 2 Futbol 11','Prueba2Futbol11','Prueba 2 Futbol 11','2018-07-29','0000-00-00','0000-00-00',0,0,1,'2018-08-05 20:45:20','2018-08-05 20:45:20'),(155,3,7,6,'Prueba 1 Futbol 9 1','Prueba1Futbol91','Prueba 1 Futbol 9 1','2018-08-05','0000-00-00','0000-00-00',1,1,1,'2018-08-05 20:45:40','2018-08-05 20:45:40'),(156,6,7,6,'Prueba 2 Futbol 9 1','Prueba2Futbol91','Prueba 2 Futbol 9 1','2018-08-05','0000-00-00','0000-00-00',0,1,1,'2018-08-05 20:46:00','2018-08-05 20:46:00'),(157,4,7,7,'Prueba 1 Futbol 9 2','Prueba1Futbol91','Prueba 1 Futbol 9 1','2018-08-05','0000-00-00','0000-00-00',0,1,1,'2018-08-05 20:46:15','2018-08-05 20:46:15'),(160,1,1,1,'lunes','lunes','lunes','2018-08-06','0000-00-00','0000-00-00',0,1,1,'2018-08-06 00:36:38','2018-08-06 00:36:38'),(161,2,1,1,'lunes','lunes','lunes','2018-08-06','0000-00-00','0000-00-00',0,0,1,'2018-08-06 00:36:45','2018-08-06 00:36:45'),(162,5,1,5,'lunes','lunes','lunes','2018-08-06','0000-00-00','0000-00-00',0,1,1,'2018-08-06 00:37:04','2018-08-06 00:37:04'),(163,9,1,5,'lunes','lunes','lunes','2018-08-06','0000-00-00','0000-00-00',0,1,1,'2018-08-06 00:37:10','2018-08-06 00:37:10'),(164,4,1,6,'lunes','lunes','lunes','2018-08-06','0000-00-00','0000-00-00',0,1,1,'2018-08-06 00:38:28','2018-08-06 00:38:28');
/*!40000 ALTER TABLE `reservas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservas_dia`
--

DROP TABLE IF EXISTS `reservas_dia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservas_dia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idReserva` int(11) NOT NULL,
  `idDia` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservas_dia`
--

LOCK TABLES `reservas_dia` WRITE;
/*!40000 ALTER TABLE `reservas_dia` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservas_dia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicios`
--

DROP TABLE IF EXISTS `servicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipoServicio` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicios`
--

LOCK TABLES `servicios` WRITE;
/*!40000 ALTER TABLE `servicios` DISABLE KEYS */;
INSERT INTO `servicios` VALUES (2,'Parrillero N°1',10,800,NULL,'2018-07-08 07:28:32','2018-07-08 07:28:32'),(3,'asdas',10,123123,NULL,'2018-07-09 08:44:22','2018-07-09 08:44:22');
/*!40000 ALTER TABLE `servicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_canchas`
--

DROP TABLE IF EXISTS `tipo_canchas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_canchas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_canchas`
--

LOCK TABLES `tipo_canchas` WRITE;
/*!40000 ALTER TABLE `tipo_canchas` DISABLE KEYS */;
INSERT INTO `tipo_canchas` VALUES (7,'Futbol 5','2018-07-07 07:22:00','2018-07-07 07:22:00'),(8,'asdas','2018-07-07 07:31:17','2018-07-07 07:31:17'),(9,'sdfsdfsdf','2018-07-09 09:00:50','2018-07-09 09:00:50');
/*!40000 ALTER TABLE `tipo_canchas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_pastos`
--

DROP TABLE IF EXISTS `tipo_pastos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_pastos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_pastos`
--

LOCK TABLES `tipo_pastos` WRITE;
/*!40000 ALTER TABLE `tipo_pastos` DISABLE KEYS */;
INSERT INTO `tipo_pastos` VALUES (4,'Natural','2018-06-26 00:57:27','2018-06-26 00:57:27'),(5,'Sintético','2018-07-07 07:24:23','2018-07-07 07:24:23'),(6,'asdasdasd','2018-07-07 07:36:16','2018-07-07 07:36:16'),(7,'asdsa','2018-07-07 07:39:33','2018-07-07 07:39:33');
/*!40000 ALTER TABLE `tipo_pastos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_servicios`
--

DROP TABLE IF EXISTS `tipo_servicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_servicios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_servicios`
--

LOCK TABLES `tipo_servicios` WRITE;
/*!40000 ALTER TABLE `tipo_servicios` DISABLE KEYS */;
INSERT INTO `tipo_servicios` VALUES (10,'asd','2018-07-08 07:28:10','2018-07-08 07:28:10'),(11,'hfgfgfghfghfghgf','2018-07-09 06:29:57','2018-07-09 06:29:57');
/*!40000 ALTER TABLE `tipo_servicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'andres','andres@infranetworking.com',1,'$2y$10$hWyXqkAtZai7ia/cBTfQT.Qlp54.swxXFNdNNGUQSSTE.XvHrrWMW','fNAoCtm5IiQX0rlEAl27A8Xa9rn8PBd2rM4oRiV0K6Lvy4CmLkKArBGr18eM','2018-04-22 07:44:12','2018-04-22 07:44:12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-06 14:41:19
