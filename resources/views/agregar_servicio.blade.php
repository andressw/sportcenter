@include('layouts.head_panel')


<div class="contentCenter columns large-10 large-offset-2 medium-12 small-12">
  <div class="contentFormulario columns large-7 medium-12 small-12">
  @if (count($tipoS) > 0)
  <label class="labelNameInput" for="nombreS">Nombre
      <input id="nombreS" type="text" name="nombreS" placeholder="Ej: Parrillero Fondo">
    </label>

    <label class="labelNameInput">Tipo de servicio
    <select id="selectTipoS">
    @for ($i = 0; $i < count($tipoS); $i++)
    <option value="{{$tipoS[$i]->id}}">{{$tipoS[$i]->nombre}}</option>
    @endfor
  </select>
    </label>    

    <label class="labelNameInput" for="precioS">Precio
      <input id="precioS" type="number" name="precioS" placeholder="Ej: 300">
    </label>    
    <button class="buttonCustomSuccess button success enviarAgregarListar" id="agregarServicio" type="button" name="agregarServicio">Agregar</button>
@else
<div class="noRegistrosIngresados columns large-12 medium-12 small-12">
      <span>Deben existir un tipo de servicio ingresado.</span>
    </div>
@endif
  </div>

</div>
@include('layouts.footer_panel')