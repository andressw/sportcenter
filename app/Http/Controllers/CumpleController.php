<?php

namespace App\Http\Controllers;

use App\Reserva;
use DB;
use Illuminate\Http\Request;

class CumpleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function darFechaEntreLunesDomingo($fecha)
    {
        $lunes = strtotime("last monday");
        $lunes = date('w', $lunes) == date('w') ? $lunes + 7 * 86400 : $lunes;

        $domingo = strtotime(date("Y-m-d", $lunes) . " +6 days");
        $flag = false;
        if ($fecha >= date("Y-m-d", $lunes) && $fecha <= date("Y-m-d", $domingo)) {
            $flag = true;
        }
        return $flag;
    }

    public function darReserva($idCancha, $idDia, $fechaDia = null, $soloDia = 1)
    {
        if ($fechaDia == null) {
            $lunes = strtotime("last monday");
            $lunes = date('w', $lunes) == date('w') ? $lunes + 7 * 86400 : $lunes;
            $dia = strtotime(date("Y-m-d", $lunes) . " +" . ($idDia - 1) . " days");
            $fechaDia = date("Y-m-d", $dia);
        }

        $result = '';

        if ($soloDia == 1) {
            $result = DB::select("select * from (select `hora_dia`.`hora`, `hora_dia`.`id` as `idHora`, `reservas`.`id` as `idReserva`,`reservas`.`idDia`, `reservas`.`nombre`,  `reservas`.`telefono`, `reservas`.`comentario`, `reservas`.`jugado`, `reservas`.`hoyJuega`, `reservas`.`soloHoy` from `hora_dia` left join `reservas` on `hora_dia`.`id` = `reservas`.`idHora` where (`reservas`.`soloHoy` = 0 and `reservas`.`idCancha` = " . $idCancha . " and `reservas`.`idDia` = " . $idDia . " and `reservas`.`fecha` <= '" . $fechaDia . "' and '" . $fechaDia . "' not BETWEEN `reservas`.`fechaDesdeNoJuega` AND `reservas`.`fechaHastaNoJuega`) UNION select `hora_dia`.`hora`, `hora_dia`.`id` as `idHora`, `reservas`.`id` as `idReserva`,`reservas`.`idDia`, `reservas`.`nombre`, `reservas`.`telefono`, `reservas`.`comentario`, `reservas`.`jugado`, `reservas`.`hoyJuega`, `reservas`.`soloHoy` from `hora_dia` left join `reservas` on `hora_dia`.`id` = `reservas`.`idHora` where (`reservas`.`soloHoy` = 1 and `reservas`.`idCancha` = " . $idCancha . " and `reservas`.`idDia` = " . $idDia . " and `reservas`.`fecha` = '" . $fechaDia . "') UNION select `hora_dia`.`hora`, `hora_dia`.`id` as `idHora`, 'idReserva' = null as 'idReserva','idDia' = null as 'idDia', 'nombre' = null as 'nombre', 'telefono' = null as 'telefono', 'comentario' = null as 'comentario', 'jugado' = null as 'jugado', 'hoyJuega' = null as 'hoyJuega', 'soloHoy' = null as 'soloHoy' from hora_dia ) as t1 group by t1.idHora order by idHora");
        } else {
            $result = DB::select("select * from (select `hora_dia`.`hora`, `hora_dia`.`id` as `idHora`, `reservas`.`id` as `idReserva`,`reservas`.`idDia`, `reservas`.`nombre`,  `reservas`.`telefono`, `reservas`.`comentario`, `reservas`.`jugado`, `reservas`.`hoyJuega`, `reservas`.`soloHoy` from `hora_dia` left join `reservas` on `hora_dia`.`id` = `reservas`.`idHora` where (`reservas`.`soloHoy` = 0 and `reservas`.`idCancha` = " . $idCancha . " and `reservas`.`idDia` = " . $idDia . ") UNION select `hora_dia`.`hora`, `hora_dia`.`id` as `idHora`, `reservas`.`id` as `idReserva`,`reservas`.`idDia`, `reservas`.`nombre`, `reservas`.`telefono`, `reservas`.`comentario`, `reservas`.`jugado`, `reservas`.`hoyJuega`, `reservas`.`soloHoy` from `hora_dia` left join `reservas` on `hora_dia`.`id` = `reservas`.`idHora` where (`reservas`.`soloHoy` = 1 and `reservas`.`idCancha` = " . $idCancha . " and `reservas`.`idDia` = " . $idDia . " and `reservas`.`fecha` = '" . $fechaDia . "') UNION select `hora_dia`.`hora`, `hora_dia`.`id` as `idHora`, 'idReserva' = null as 'idReserva','idDia' = null as 'idDia', 'nombre' = null as 'nombre', 'telefono' = null as 'telefono', 'comentario' = null as 'comentario', 'jugado' = null as 'jugado', 'hoyJuega' = null as 'hoyJuega', 'soloHoy' = null as 'soloHoy' from hora_dia ) as t1 group by t1.idHora order by idHora");
        }
        return ["fecha" => $fechaDia, "result" => $result];
    }

    public function cargarWeb()
    {//DB::enableQueryLog();
        

        $cancha = DB::table('canchas')->where("permiteCumpleanos", 1)->orderby("id")->get();
        $dias = DB::table('dia_semana')->orderby("id")->get();
        $horas = DB::table('hora_dia')->orderby("id")->get();

        $reservas = $this->darReserva($cancha[0]->id, $dias[date('N') - 1]->id);
        $fechaDiaMes = explode('-', $reservas["fecha"]);
        $canchaDia = $cancha[0]->nombre . " - " . $dias[date('N') - 1]->nombre;
    //dd(DB::getQueryLog());
return view('cumple', ['seccion' => 'cumple', "fechaDiaMes" => $fechaDiaMes, "tituloSección" => 'Cumpleaños', "fecha" => $reservas["fecha"], "idCancha" => $cancha[0]->id, "reservas" => $reservas["result"], "horas" => $horas, "canchas" => $cancha, "canchaDia" => $canchaDia]);

    }

    public function cargarReservasCancha(Request $request)
    {
        $cancha = DB::table('canchas')->where("id", $request->id)->get();
        $dias = DB::table('dia_semana')->orderby("id")->get();
        $diaSelected = (isset($request->dia) && $request->dia != -1) ? $request->dia : date('N');
        $reservas = $this->darReserva($cancha[0]->id, $dias[$diaSelected - 1]->id);
        $fechaDiaMes = explode('-', $reservas["fecha"]);
        $canchaDia = $cancha[0]->nombre . " - " . $dias[$diaSelected - 1]->nombre . " <small>(" . $fechaDiaMes[2] . "/" . $fechaDiaMes[1] . ")</small>";

        $resultado = '';
        $horasDisponibles = '';
        for ($i = 0; $i < count($reservas["result"]); $i++) {
            $resultado = ($reservas["result"][$i]->soloHoy = !null && $reservas["result"][$i]->soloHoy = !'' && $reservas["result"][$i]->soloHoy == 1) ? $resultado . '<tr class="soloDia">' : $resultado . '<tr>';
            $resultado = $resultado . '<td>' . substr($reservas["result"][$i]->hora, 0, -3) . '</td><td>' . $reservas["result"][$i]->nombre . '</td><td>' . $reservas["result"][$i]->telefono . '</td><td>' . $reservas["result"][$i]->comentario . '</td>';
            /**Aca genero el listado de las horas disponibles para el día ademas de las reservas*/
            if ($reservas["result"][$i]->idReserva != null && $reservas["result"][$i]->soloHoy == 1) {
                if ($reservas["result"][$i]->jugado == 1) {
                    $resultado = $resultado . '<td><input id="jugo' . $reservas["result"][$i]->idReserva . '" type="checkbox" checked onclick="marcarComoJugado(this, ' . $reservas["result"][$i]->idReserva . ');"><label for="jugo' . $reservas["result"][$i]->idReserva . '">¿Jugo?</label></td>';
                } else {
                    $resultado = $resultado . '<td><input id="jugo' . $reservas["result"][$i]->idReserva . '" type="checkbox" onclick="marcarComoJugado(this, ' . $reservas["result"][$i]->idReserva . ');"><label for="jugo' . $reservas["result"][$i]->idReserva . '">¿Jugo?</label></td>';
                }
                $resultado = $resultado . '<td><i class="fa fa-edit" onclick="modificarReserva(' . $reservas["result"][$i]->idHora . ',' . $reservas["result"][$i]->idReserva . ');"></i><i class="fas fa-trash-alt" onclick="eliminarReserva(' . $reservas["result"][$i]->idHora . ',' . $reservas["result"][$i]->idReserva . ');"></i><input type="hidden" name="idReserva' . $reservas["result"][$i]->idReserva . '" />';
                $horasDisponibles = $horasDisponibles . '<option value="' . $reservas["result"][$i]->idHora . '" disabled >' . substr($reservas["result"][$i]->hora, 0, -3) . ' (por día)</option>';
            } elseif ($reservas["result"][$i]->idReserva != null && $reservas["result"][$i]->soloHoy == 0) {
                if ($reservas["result"][$i]->jugado == 1) {
                    $resultado = $resultado . '<td><input id="jugo' . $reservas["result"][$i]->idReserva . '" type="checkbox" checked onclick="marcarComoJugado(this, ' . $reservas["result"][$i]->idReserva . ');"><label for="jugo' . $reservas["result"][$i]->idReserva . '">¿Jugo?</label></td>';
                } else {
                    $resultado = $resultado . '<td><input id="jugo' . $reservas["result"][$i]->idReserva . '" type="checkbox" onclick="marcarComoJugado(this, ' . $reservas["result"][$i]->idReserva . ');"><label for="jugo' . $reservas["result"][$i]->idReserva . '">¿Jugo?</label></td>';
                }
                $resultado = $resultado . '<td><i class="fa fa-edit" onclick="modificarReserva(' . $reservas["result"][$i]->idHora . ',' . $reservas["result"][$i]->idReserva . ');"></i><i class="fas fa-trash-alt" onclick="eliminarReserva(' . $reservas["result"][$i]->idHora . ',' . $reservas["result"][$i]->idReserva . ');"></i><input type="hidden" name="idReserva' . $reservas["result"][$i]->idReserva . '" />';
                $horasDisponibles = $horasDisponibles . '<option value="' . $reservas["result"][$i]->idHora . '" disabled >' . substr($reservas["result"][$i]->hora, 0, -3) . ' (semanal)</option>';
            } else {
                $resultado = $resultado . '<td></td><td><i class="fa fa-plus-circle" onclick="agregarReserva(this,' . $reservas["result"][$i]->idHora . ',' . $cancha[0]->id . ');"></i>';
                $horasDisponibles = $horasDisponibles . '<option value="' . $reservas["result"][$i]->idHora . '" >' . substr($reservas["result"][$i]->hora, 0, -3) . '</option>';

            }
            $resultado = $resultado . '<input type="hidden" name="idTr' . $reservas["result"][$i]->idHora . '" /></td></tr>';

        }

        return response(["horasDisponibles" => $horasDisponibles, "fechaEsMenor" => (strtotime($reservas["fecha"]) >= strtotime(date('Y-m-d'))) ? $reservas["fecha"] : date('Y-m-d'), "resultado" => $resultado, "canchaDia" => $canchaDia, "fecha" => $reservas["fecha"]], 200)
            ->header('Content-Type', 'json');

        //return view('reserva', ['seccion' => 'reservas', "tituloSección" => 'Reservas',"fecha" => $reservas["fecha"], "idCancha" => $cancha[0]->id, "reservas" => $reservas["result"],"horas" => $horas, "canchas" => $cancha, "dias" => $dias, "canchaDia"=> $canchaDia]);
    }

    public function darHorasFecha(Request $request)
    {
        //$cancha = DB::table('canchas')->where("id", $request->id)->get();
        $reservas = $this->darReserva($request->cancha, date('N', strtotime($request->fecha)), $request->fecha, $request->soloDia);
        $horasDisponibles = '';
        for ($i = 0; $i < count($reservas["result"]); $i++) {
            if ($reservas["result"][$i]->idReserva != null && $reservas["result"][$i]->soloHoy == 1) {
                $horasDisponibles = $horasDisponibles . '<option value="' . $reservas["result"][$i]->idHora . '" disabled >' . substr($reservas["result"][$i]->hora, 0, -3) . ' (por día)</option>';
            } elseif ($reservas["result"][$i]->idReserva != null && $reservas["result"][$i]->soloHoy == 0) {
                $horasDisponibles = $horasDisponibles . '<option value="' . $reservas["result"][$i]->idHora . '" disabled >' . substr($reservas["result"][$i]->hora, 0, -3) . ' (semanal)</option>';
            } else {

                $horasDisponibles = $horasDisponibles . '<option value="' . $reservas["result"][$i]->idHora . '" >' . substr($reservas["result"][$i]->hora, 0, -3) . '</option>';
            }
        }
        return response(["horasDisponibles" => $horasDisponibles], 200)
            ->header('Content-Type', 'json');
    }

    public function cargaarVistaHistorial()
    {
        return view('historial_reservas', ['seccion' => 'historialReservas', "tituloSección" => 'Historial de reservas']);

    }

    public function corroborarFechaSoloDiaParaSemanal(Request $request)
    {
        $sePuedeSemanal = true;
        $texto = '';
        $result = DB::table('reservas')->where([
            ['idDia', '=', date('N', strtotime($request->fecha))],
            ['fecha', '>=', $request->fecha],
            ['idHora', '=', $request->hora],
            ['idCancha', '=', $request->cancha],
            ['soloHoy', '=', '1'],
        ])->whereNotBetween('fecha', [$request->fechaDesde, $request->fechaHasta])->get();
        if (count($result) > 0) {
            $sePuedeSemanal = false;
            $texto = "Existen reservas agendadas para esta hora y día de la semana, las fechas son las siguientes:";
            for ($i = 0; $i < count($result); $i++) {
                $texto = $texto . " " . $result[$i]->fecha . ",";
            }
            $texto = $texto . " ¿Desea continuar de todas formas y eliminar dichas reservas?";
        }
        return response(["resultado" => $sePuedeSemanal, "texto" => $texto], 200)
            ->header('Content-Type', 'json');

    }

    public function cambiarJugoReserva(Request $request)
    {
        DB::table('reservas')
            ->where('id', $request->id)
            ->update(['jugado' => $request->jugo]);
    }

    public function actualizarReservaSoloDia(Request $request)
    {
        $this->validate($request, ["cancha" => 'required|min:1',
            "dpFecha" => 'required|min:1',
            "horasReserva" => 'required|min:1',
            "nombreReserva" => 'required|min:1',
            "telReserva" => 'required|min:1',
        ]);

        $reserva = new Reserva();
        $reserva->idHora = $request->horasReserva;
        $reserva->idDia = date('N', strtotime($request->dpFecha));
        $reserva->idCancha = $request->cancha;
        $reserva->nombre = $request->nombreReserva;
        $reserva->telefono = str_replace(" ", "", $request->telReserva);
        $reserva->comentario = $request->comentarioReserva;
        $reserva->fecha = $request->dpFecha;
        $reserva->jugado = 0;
        $reserva->fechaDesdeNoJuega = $request->fechaDesde;
        $reserva->fechaHastaNoJuega = $request->fechaHasta;
        $reserva->soloHoy = $request->soloDia;
        $reserva->hoyJuega = 1;



        if ($reserva->fechaDesdeNoJuega == null) {
            DB::table('reservas')->where('id', $request->idReservaHidden)->update(['idHora' => $reserva->idHora,'idDia' => $reserva->idDia,'idCancha' => $reserva->idCancha,'nombre' => $reserva->nombre,'telefono' => $reserva->telefono,'comentario' => $reserva->comentario,'fecha' => $reserva->fecha,'fechaDesdeNoJuega' => $reserva->fechaDesdeNoJuega,'fechaHastaNoJuega' => $reserva->fechaHastaNoJuega,'soloHoy' => $reserva->soloHoy, 'updated_at' => date("Y-m-d H:i:s")]);
            
                
            
                if ($request->eliminarReservas == 1) {
                    $this->eliminarReservasPorRegistroSemanal($request->dpFecha, $request->horasReserva, $request->cancha, $request->fechaDesde, $request->fechaHasta);
                }
                return response(["mensaje" => "Reserva actualizada correctamente.", "entreLyD" => $this->darFechaEntreLunesDomingo($request->dpFecha), "id" => DB::table('reservas')->select('id', 'idDia')->orderBy('updated_at', 'desc')->first()], 200)
                    ->header('Content-Type', 'json');
            
        } else {
            /**Para corroborar si al fecha desde es menor a la fecha hasta */
            if (strtotime($reserva->fechaDesdeNoJuega) <= strtotime($reserva->fechaHastaNoJuega)) {
                DB::table('reservas')->where('id', $request->idReservaHidden)->update(['idHora' => $reserva->idHora,'idDia' => $reserva->idDia,'idCancha' => $reserva->idCancha,'nombre' => $reserva->nombre,'telefono' => $reserva->telefono,'comentario' => $reserva->comentario,'fecha' => $reserva->fecha,'fechaDesdeNoJuega' => $reserva->fechaDesdeNoJuega,'fechaHastaNoJuega' => $reserva->fechaHastaNoJuega,'soloHoy' => $reserva->soloHoy, 'updated_at' => date("Y-m-d H:i:s")]);
                $idActualizada = DB::table('reservas')->select('id')->orderBy('updated_at', 'desc')->first();
                
                if ($idActualizada->id != $request->idReservaHidden) {
                    return response(["mensaje" => 'Asegurese de haber ingresado todos los campos requeridos.'], 500)
                        ->header('Content-Type', 'json');
                } else {

                    if ($request->eliminarReservas == 1) {
                        $this->eliminarReservasPorRegistroSemanal($request->dpFecha, $request->horasReserva, $request->cancha, $request->fechaDesde, $request->fechaHasta);
                    }

                    return response(["mensaje" => "Reserva actualizada correctamente.", "entreLyD" => $this->darFechaEntreLunesDomingo($request->dpFecha), "id" => DB::table('reservas')->select('id', 'idDia')->orderBy('updated_at', 'desc')->first()], 200)
                        ->header('Content-Type', 'json');
                }
            } else {
                return response(["mensaje" => 'La fecha Desde debe ser menor a la fecha Hasta.'], 500)
                    ->header('Content-Type', 'json');
            }
        }
    }

    public function agregarSoloPorDia(Request $request)
    {

        $this->validate($request, ["cancha" => 'required|min:1',
            "dpFecha" => 'required|min:1',
            "horasReserva" => 'required|min:1',
            "nombreReserva" => 'required|min:1',
            "telReserva" => 'required|min:1',
        ]);

        $reserva = new Reserva();
        $reserva->idHora = $request->horasReserva;
        $reserva->idDia = date('N', strtotime($request->dpFecha));
        $reserva->idCancha = $request->cancha;
        $reserva->nombre = $request->nombreReserva;
        $reserva->telefono = str_replace(" ", "", $request->telReserva);
        $reserva->comentario = $request->comentarioReserva;
        $reserva->fecha = $request->dpFecha;
        $reserva->jugado = 0;
        $reserva->fechaDesdeNoJuega = $request->fechaDesde;
        $reserva->fechaHastaNoJuega = $request->fechaHasta;
        $reserva->soloHoy = $request->soloDia;
        $reserva->hoyJuega = 1;

        if ($reserva->fechaDesdeNoJuega == null) {
            if (!$reserva->save()) {
                return response(["mensaje" => 'Asegurese de haber ingresado todos los campos requeridos.'], 500)
                    ->header('Content-Type', 'json');
            } else {
                if ($request->eliminarReservas == 1) {
                    $this->eliminarReservasPorRegistroSemanal($request->dpFecha, $request->horasReserva, $request->cancha, $request->fechaDesde, $request->fechaHasta);
                }
                return response(["mensaje" => "Reserva ingresada correctamente.", "entreLyD" => $this->darFechaEntreLunesDomingo($request->dpFecha), "id" => DB::table('reservas')->select('id', 'idDia')->orderBy('created_at', 'desc')->first()], 200)
                    ->header('Content-Type', 'json');
            }
        } else {
            /**Para corroborar si al fecha desde es menor a la fecha hasta */
            if (strtotime($reserva->fechaDesdeNoJuega) <= strtotime($reserva->fechaHastaNoJuega)) {
                if (!$reserva->save()) {
                    return response(["mensaje" => 'Asegurese de haber ingresado todos los campos requeridos.'], 500)
                        ->header('Content-Type', 'json');
                } else {

                    if ($request->eliminarReservas == 1) {
                        $this->eliminarReservasPorRegistroSemanal($request->dpFecha, $request->horasReserva, $request->cancha, $request->fechaDesde, $request->fechaHasta);
                    }

                    return response(["mensaje" => "Reserva ingresada correctamente.", "entreLyD" => $this->darFechaEntreLunesDomingo($request->dpFecha), "id" => DB::table('reservas')->select('id', 'idDia')->orderBy('created_at', 'desc')->first()], 200)
                        ->header('Content-Type', 'json');
                }
            } else {
                return response(["mensaje" => 'La fecha Desde debe ser menor a la fecha Hasta.'], 500)
                    ->header('Content-Type', 'json');
            }
        }

    }

    public function chequearPersonaEnListaNegra(Request $request)
    {

        $persona = DB::table('lista_negras')->where('telefono', str_replace(" ", "", $request->tel))->get();
        return count($persona);
    }

    public function eliminarReserva(Request $request)
    {
        DB::table('reservas')->where('id', $request->id)->delete();
    }

    public function eliminarReservasPorRegistroSemanal($fecha, $hora, $cancha, $fechaDesde, $fechaHasta)
    {
        //Arreglar para eliminar las reservas por día
        //https://stackoverflow.com/questions/16815551/how-to-do-this-in-laravel-subquery-where-in

        //
        //DB::select('delete from reservas where id in (SELECT id FROM `reservas` WHERE `idDia` = '.date('N', strtotime($fecha)).' and `fecha` >= '.$fecha.' and idHora = '.$hora.' and `idCancha` = '.$cancha.' and `soloHoy` = 1)');

        $result = DB::table('reservas')->select("id")->where([
            ['idDia', '=', date('N', strtotime($fecha))],
            ['fecha', '>=', $fecha],
            ['idHora', '=', $hora],
            ['idCancha', '=', $cancha],
            ['soloHoy', '=', '1'],
        ])->whereNotBetween('fecha', [$fechaDesde, $fechaHasta])->get();
        $datos = [];
        for ($i = 0; $i < count($result); $i++) {
            $datos[$i] = $result[$i]->id;
        }

        DB::table('reservas')->whereIn('id', $datos)->delete();
    }

    public function getRegistroModificarReserva(Request $request)
    {
        $lista = DB::table('reservas')->where('id', $request->id)->get();
        return response(["data" => $lista], 200)
            ->header('Content-Type', 'json');
    }

}
