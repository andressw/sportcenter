<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaFecha extends Model
{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'idHora', 'idCancha', 'nombre', 'telefono', 'comentario', 'fechaDesdeNoJuega', 'fechaHastaNoJuega', 'jugo'
  ];
}
