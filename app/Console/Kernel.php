<?php

namespace App\Console;

use DB;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $idDiaHoy = date('N') ;
            $lunes = strtotime("last monday");
            $lunes = date('w', $lunes) == date('w') ? $lunes + 7 * 86400 : $lunes;
            $dia = strtotime(date("Y-m-d", $lunes) . " +" . ($idDiaHoy - 2) . " days");
            $fechaDia = date("Y-m-d", $dia);
            $idDia = date('N',$dia) ;
            //$reservas = DB::select("select * from (select `reservas`.`idHora` as `idHora`, `reservas`.`id` as `idReserva`,`reservas`.`idDia`,`reservas`.`idCancha`, `reservas`.`nombre`,  `reservas`.`telefono`, `reservas`.`comentario`, `reservas`.`jugado`, `reservas`.`soloHoy`, reservas.fecha as fecha from `reservas`) as t1");
            $reservas = DB::select("select * from (select `reservas`.`idHora` as `idHora`, `reservas`.`id` as `idReserva`,`reservas`.`idDia`,`reservas`.`idCancha`, `reservas`.`nombre`,  `reservas`.`telefono`, `reservas`.`comentario`, `reservas`.`jugado`, `reservas`.`soloHoy`, reservas.fecha as fecha from `reservas` where (`reservas`.`soloHoy` = 0 and `reservas`.`idDia` = " . $idDia . " and `reservas`.`fecha` <= '" . $fechaDia . "' and '" . $fechaDia . "' not BETWEEN `reservas`.`fechaDesdeNoJuega` AND `reservas`.`fechaHastaNoJuega`) UNION select `reservas`.`idHora` as `idHora`, `reservas`.`id` as `idReserva`,`reservas`.`idDia`,`reservas`.`idCancha`, `reservas`.`nombre`,  `reservas`.`telefono`, `reservas`.`comentario`, `reservas`.`jugado`, `reservas`.`soloHoy`, reservas.fecha as fecha from `reservas` where (`reservas`.`soloHoy` = 1 and `reservas`.`idDia` = " . $idDia . " and `reservas`.`fecha` = '" . $fechaDia . "')) as t1 order by idReserva");
            $arrayInserts = [];
            for ($i = 0; $i < count($reservas); $i++) {

                $arrayInserts[$i] = [
                    'idHora' => $reservas[$i]->idHora,
                    'idReserva' => $reservas[$i]->idReserva,
                    'idDia' => $reservas[$i]->idDia,
                    'idCancha' => $reservas[$i]->idCancha,
                    'nombre' => $reservas[$i]->nombre,
                    'telefono' => $reservas[$i]->telefono,
                    'comentario' => $reservas[$i]->comentario,
                    'jugado' => $reservas[$i]->jugado,
                    'soloHoy' => $reservas[$i]->soloHoy,
                    'fecha' => $fechaDia,

                ];

            }

            DB::table('historial')->insert(
                $arrayInserts
            );
        })->timezone('America/Montevideo')->daily();
        
         
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
